# Elliptical automatic mode simulator - Simple version without technological background.

import time

from time import sleep
from opc_client import OPCClient

# Variables
start_time = time.time()
plc_ip = "10.0.0.25"
opc_addr = "opc.tcp://" + plc_ip + ":4840"
client = OPCClient(opc_addr, 10)

# Functions
def PIDsim(mYclient, actSP, actFB, sensMV):
    setpoint = OPCClient.getValue(mYclient, actSP)
    sleep(0.05)
    measValue = OPCClient.getValue(mYclient, sensMV)
    if((setpoint > 0) and measValue <= 26748):
      measValue += int(round(setpoint / 1000))
    elif(measValue >= 27648):
      measValue = 27640
    elif((setpoint <= 0) and measValue > 0):
      measValue -= int(round(10))
    elif(measValue <= 0):
      measValue = 1
    OPCClient.setValue(mYclient, sensMV, measValue)
    OPCClient.setValue(mYclient, actFB, setpoint)
    sleep(0.01)


def main():
   # Connect to the PLC
   client.connect()

   # Get OPC nodes
   TS2_CV001_FB = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Inputs', '3:TS2-010CRM:Cryo-CV-001'])
   TS2_CV001_SP = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Outputs', '3:TS2-010CRM:Cryo-CV-001AO'])
   TS2_LT001 = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Inputs', '3:TS2-010CRM:Cryo-LT-001'])

   while KeyboardInterrupt(" error"):
       PIDsim(client, TS2_CV001_SP, TS2_CV001_FB, TS2_LT001)
       sleep(0.1)
       print("Runtime: --- %s seconds ---" % (time.time() - start_time))

if __name__ == '__main__':
   main()
