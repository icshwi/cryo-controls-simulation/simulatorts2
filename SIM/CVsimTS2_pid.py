from time import sleep
from opc_client import OPCClient

import time
start_time = time.time()

plc_ip = "10.0.0.25"
opc_addr = "opc.tcp://" + plc_ip + ":4840"
client = OPCClient(opc_addr, 10)

client.connect()


def PIDsim(mYclient, actSP, actFB, sensMV):
    actSP = OPCClient.getValue(mYclient, actSP)
    sleep(0.05)
    measValue = OPCClient.getValue(mYclient, sensMV)
    if(actSP > 50):
        measValue += 100
    elif((actSP < 50) and measValue != 0):
        measValue -= 100
    OPCClient.setValue(mYclient, sensMV, measValue)
    OPCClient.setValue(mYclient, actFB, actSP)
    sleep(0.01)


# TS2:
# Get OPC nodes
TS2_CV001_FB = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Inputs', '3:TS2-010CRM:Cryo-CV-001'])
TS2_CV001_SP = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Outputs', '3:TS2-010CRM:Cryo-CV-001AO'])
TS2_LT001 = client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:Inputs', '3:TS2-010CRM:Cryo-LT-001'])

while KeyboardInterrupt(" error"):
    PIDsim(client, TS2_CV001_SP, TS2_CV001_FB, TS2_LT001)
    print("--- %s seconds ---" % (time.time() - start_time))



'''
# v0.01
while KeyboardInterrupt:
    #TS2:
    TS2_temp1 = OPCClient.getValue(client, TS2_CV001_SP)
    print(TS2_temp1)
    sleep(0.05)
    TS2_pt_temp = OPCClient.getValue(client, TS2_PT002)
    if(TS2_temp1 > 50.0):
        TS2_pt_temp = TS2_pt_temp + 100
    elif(TS2_temp1 < 50.0) and (TS2_pt_temp != 0):
        TS2_pt_temp = TS2_pt_temp - 100
    OPCClient.setValue(client, TS2_PT002, TS2_pt_temp)
    OPCClient.setValue(client, TS2_CV001_FB, TS2_temp1)
    sleep(0.01)
'''
