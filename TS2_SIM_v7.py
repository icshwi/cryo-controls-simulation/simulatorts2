#import warnings
#from cryptography.utils import CryptographyDeprecationWarning
#warnings.filterwarnings("ignore", category=CryptographyDeprecationWarning)
#warnings.filterwarnings(action='ignore',module='.*paramiko.*')
from concurrent.futures import thread
from crypt import methods
from doctest import master
from email.mime import application
import imp
from math import fabs
from math import isclose
from mimetypes import init
from multiprocessing.connection import Client
from tkinter import *
import tkinter
from tkinter import ttk
#from turtle import bgcolor, ondrag
from types import FrameType
from unittest import result
import pygubu
import multiprocessing
import threading
import time
import logging
from time import sleep
from datetime import datetime

from pygubu import builder
from opc_client import OPCClient
from TS2app import *
from DeviceTypes import *
from myLib import *
from epics import caget
import numpy as np
import random
import inspect
import re
import sys
#from queues import Queue

#from test_threading import print_square

class actStateHandler(object):
#region ─── OPC INIT ───────────────────────────────────────────────────────────────────
    def __init__(self):
        self.state = 0
        self.substate = 0
        self.subsubstate = 0
        self.app = application
        self.names = app.names
        self.states = app.states
#endregion

#region ─── OPC METHODS ────────────────────────────────────────────────────────────────
    def datachange_notification(self, node, val, data):

        self.state, self.substate, self.subsubstate = app.stateMachine(val)

        #app.setLabel('stateid_label', 'ID: ' + str(self.state) + ' | subID: ' + str(self.substate) + ' | subsubID: ' + str(self.subsubstate))
        app.setLabel('stateid_label', str(self.state) + ' : ' + str(self.substate) + ' : ' + str(self.subsubstate))

        for name in self.names:
            if name != None:
                index = self.names.index(name)
                exist = app.checkItem('frame'+str(index))
                if exist:
                    for state in self.states[index]:
                        st, subst, subsubst = app.stateMachine(state)
                        if st == self.state and subst == self.substate and subsubst == self.subsubstate:
                            app.setColor('frame'+str(index),'yellow')
                            break
                        else:
                            app.setColor('frame'+str(index),'#d9d9d9')
#endregion

#region ─── CHECK STATE ────────────────────────────────────────────────────────────────
def check_state(ID,subID,subsubID,IDval,subIDval,subsubIDval):
    if (ID == IDval or IDval == 0) and (subID == subIDval or subIDval == 0) and (subsubID == subsubIDval or subsubIDval == 0):
        return True
    else:
        return False
#endregion

#region ─── COLORING THREAD ────────────────────────────────────────────────────────────
# def feedbackColor(quit_flag, lista):
#     while not quit_flag.value:
#         time.sleep(0.5)      
#         if len(lista) > 2:
#             if isinstance(lista[0], float) or isinstance(lista[0], int):
#                 if lista[0] >= 20:
#                     if lista[1] == lista[2]:
#                         app.setColor('label' + str(int(lista[0])), '#d9d9d9')
#                     else:
#                         app.setColor('label' + str(int(lista[0])), 'red')
#endregion

#region ─── LABEL COLORING ─────────────────────────────────────────────────────────────
def labelColor(index, color):
    if color == 1:
        app.setColor('label' + str(index), '#d9d9d9')
    elif color == 2:
        app.setColor('label' + str(index), 'red')
    elif color == 3:
        app.setColor('label' + str(index), 'cyan')

#endregion

def worker_function(app, quit_flag):
# def worker_function(app, quit_flag, lista):
    #region ─── DECLARATIONS ───────────────────────────────────────────────────────────────
    plcName = '3:TS2_LCS'
    delayTime = 0.0025
    # empty = [None] * 3
    # lista.extend(empty)

    saveEntrys = []
    saveEntrys.extend(app.names)
    for i in range(0, len(saveEntrys)):
        saveEntrys[i] = 0.0

    setColor = []
    setColor.extend(app.names)
    for i in range(0, len(setColor)):
        setColor[i] = 0

    saveColor = []
    saveColor.extend(app.names)
    for i in range(0, len(saveColor)):
        saveColor[i] = 0

    actState = app.actState
    actSubState = app.actSubState
    actSubSubState = app.actSubSubState

    newStateEvent = False

    #endregion

    #region ─── CONNECT TO PLC: ────────────────────────────────────────────────────────────
    client = OPCClient("10.0.0.25", timeout=10)
    #client = OPCClient("172.30.6.222", timeout=10)  
    client.connect()
    #endregion

    #region ─── CREATE SUBSCRIPTION ────────────────────────────────────────────────────────
    try:    
        stateHandler = actStateHandler()
        st = client.create_subscription(500, stateHandler)
        actStateNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:ss_StateIdToSim'])
        actStateHandle = st.subscribe_data_change(actStateNode)
        time.sleep(0.1)
        st.subscribe_events()
    except Exception as e:
        print('subscription: ', e)
    #endregion

    #region ─── CALL DEVICE TYPES: ──────────────────────────────────────────────────────────

    #endregion

    #─── SIMULATION: ─────────────────────────────────────────────────────────────────
    # ────────────────────────────────────────────────────────────────────────────────
    cabtr1sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR1', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cabtr1sim, True)
    cabtr2sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR2', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cabtr2sim, True)
    cabtr3sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR3', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cabtr3sim, True)
    cabtr4sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR4', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cabtr4sim, True)
    cabtr5sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR5', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cabtr5sim, True)

    cernox1sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82305', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cernox1sim, True)
    cernox2sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82306', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, cernox2sim, True)

    pt100sim1 = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82360', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, pt100sim1, True)
    pt100sim2 = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE-82365', '3:Inputs', '3:Simulation'])
    OPCClient.setValue(client, pt100sim2, True)

    #counter = 0
    while not quit_flag.value:
        #counter += self.units
        #logging.info("Work # %d" % counter)

        time.sleep(0.1)

        #region ─── NOISE GENERATOR ─────────────────────────────────────────────
        #noise = np.random.normal(0,2,10)
        #endregion
                
        # lista[0] = index
        # lista[1] = int(entry)
        # lista[2] = saveEntrys[index]

        #region ─── STATE MACHINE ───────────────────────────────────────────────
        if actState != app.actState:
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:si_StateID'])
            OPCClient.setValue(client, obj, app.actState)
            actState = OPCClient.getValue(client, obj)
            newStateEvent = True

        if actSubState != app.actSubState:
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:si_SubStateID'])
            OPCClient.setValue(client, obj, app.actSubState)
            actSubState = OPCClient.getValue(client, obj)
            newStateEvent = True

        if actSubSubState != app.actSubSubState:
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:si_SubSubStateID'])
            OPCClient.setValue(client, obj, app.actSubSubState)
            actSubSubState = OPCClient.getValue(client, obj)
            newStateEvent = True
        
        if newStateEvent:
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:ss_StateName'])
            app.actStateName = OPCClient.getValue(client, obj)
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:ss_SubStateName'])
            app.actSubStateName = OPCClient.getValue(client, obj)
            obj = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:___States_DB', '3:Static', '3:ss_SubSubStateName'])
            app.actSubSubStateName = OPCClient.getValue(client, obj)
            if app.actStateName != "" and app.actSubStateName != "" and app.actSubSubStateName != "":
                app.setLabel('statename_label', str(app.actStateName) + ' -> ' + str(app.actSubStateName) + ' -> ' + str(app.actSubSubStateName))
            elif app.actStateName != "" and app.actSubStateName != "":
                app.setLabel('statename_label', str(app.actStateName) + ' -> ' + str(app.actSubStateName))
            elif app.actStateName != "":
                app.setLabel('statename_label', str(app.actStateName))
            else:
                app.setLabel('statename_label', "no info")
            newStateEvent = False
        #endregion

        #region ─── DEVICE TYPES CONTROL ───────────────────────────────────────────────
        if app.InitReady == False:
            if app.FirstRun == True:
                app.setBusyCursor()
            else:
                app.setDefCursor()
                app.InitReady = True

        for index, typ in enumerate(app.types):

            if typ == 'analog':
                if app.FirstRun:
                    if app.ValuesFromPLC:
                        val = GetVal(client, plcName, app.paths[index])
                        val = scale(val, app.scmins[index], app.scmaxs[index])
                        app.setEntryScale(index, val)
                    else:
                        entry = app.saventrys[index]
                        app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    entry = unscale(entry, app.scmins[index], app.scmaxs[index])
                    
                    if saveEntrys[index] != int(entry): #or app.FirstRun:
                        saveEntrys[index] = Analog(client, plcName, app.paths[index], int(entry))
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'digital':
                if app.FirstRun:
                    if app.ValuesFromPLC:
                        val = GetVal(client, plcName, app.paths[index])
                        app.setEntryScale(index, val)
                    else:
                        entry = app.saventrys[index]
                        app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    if entry > 0:
                        entry = 1

                    if saveEntrys[index] != int(entry): #or app.FirstRun:
                        saveEntrys[index] = Digital(client, plcName, app.paths[index], int(entry))
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'sipart':
                if app.FirstRun:
                    if app.ValuesFromPLC:
                        val = GetSipartVal(client, plcName, app.paths[index])                
                        val = 100.0 * val / (float(app.scmaxs[index]) - float(app.scmins[index]))
                        app.setEntryScale(index, val)
                    else:
                        entry = app.saventrys[index]
                        app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    entry = (float(app.scmaxs[index]) - float(app.scmins[index])) / 100.0 * entry
                    
                    if not isclose(saveEntrys[index], entry, rel_tol = 0.001): #or app.FirstRun:
                        saveEntrys[index] = Sipart(client, plcName, app.paths[index], entry)
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'dwanalog':
                if app.FirstRun:
                    if app.ValuesFromPLC:
                        val = GetVal(client, plcName, app.paths[index])
                        val = 100.0 * val / (float(app.scmaxs[index]) - float(app.scmins[index]))
                        app.setEntryScale(index, val)
                    else:
                        entry = app.saventrys[index]
                        app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    entry = (float(app.scmaxs[index]) - float(app.scmins[index])) / 100.0 * entry

                    if not isclose(saveEntrys[index], entry, rel_tol = 0.001): #or app.FirstRun:
                        saveEntrys[index] = Analog(client, plcName, app.paths[index], entry)
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'cabtr':
                if app.FirstRun:
                    entry = app.saventrys[index]
                    app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    
                    if not isclose(saveEntrys[index], entry, rel_tol = 0.001): #or app.FirstRun:
                        saveEntrys[index] = Cabtr(client, plcName, app.paths[index], entry)
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'cernox':
                if app.FirstRun:
                    entry = app.saventrys[index]
                    app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    
                    if not isclose(saveEntrys[index], entry, rel_tol = 0.001): #or app.FirstRun:
                        saveEntrys[index] = Cernox(client, plcName, app.paths[index], entry)
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]

            if typ == 'pt100':
                if app.FirstRun:
                    entry = app.saventrys[index]
                    app.setEntryScale(index, entry)
                else:
                    entry = app.entrys[index]
                    entry = limit(entry, app.lolims[index], app.hilims[index])
                    
                    if not isclose(saveEntrys[index], entry, rel_tol = 0.001): #or app.FirstRun:
                        saveEntrys[index] = Pt100(client, plcName, app.paths[index], entry)
                        setColor[index] = 2
                    else:
                        if app.savedval[index] == False:
                            setColor[index] = 3
                        else:
                            setColor[index] = 1

                    if setColor[index] != saveColor[index]:
                        labelColor(index, setColor[index])
                        saveColor[index] = setColor[index]
        
        app.FirstRun = False
        #endregion

    OPCClient.setValue(client, cabtr1sim, False)
    OPCClient.setValue(client, cabtr2sim, False)
    OPCClient.setValue(client, cabtr3sim, False)
    OPCClient.setValue(client, cabtr4sim, False)
    OPCClient.setValue(client, cabtr5sim, False)
    OPCClient.setValue(client, cernox1sim, False)
    OPCClient.setValue(client, cernox2sim, False)
    OPCClient.setValue(client, pt100sim1, False)
    OPCClient.setValue(client, pt100sim2, True)

    #region ─── UNSUBSCRIBE ────────────────────────────────────────────────────────────────
    try:
        st.unsubscribe(actStateHandle)
        time.sleep(0.1)
        st.delete()
    except Exception as e:
        print('unsubscription: ', e)
    #endregion

    #region ─── DISCONNECT FROM PLC: ───────────────────────────────────────────────────────   
    client.disconnect()
    #endregion

# def sync_function(app, quit_flag):
#     while not quit_flag.value:
#         localtime = time.localtime()
#         result = time.strftime("%I:%M:%S %p", localtime)
#         #print(result)
#         time.sleep(1)

#region ─── EVENTS ─────────────────────────────────────────────────────────────────────
def handleWheelUp(event):
    print("event: ", event.widget)
    print("focus: ", root.focus_get())
    print()

def handleWheelDown(event):
    print("event: ", event.widget)
    print("focus: ", root.focus_get())
    print()
#endregion

#region ─── MAIN ───────────────────────────────────────────────────────────────────────
if __name__ == '__main__':   

    try:    
        root = Tk()

        #root.bind("<Button-4>", handleWheelUp)
        #root.bind("<Button-5>", handleWheelDown)

        root.title("TS2sim")
        #root.geometry("500x400")
        app = Application(master=root)
        quit_flag = multiprocessing.Value('i', int(False))
        # manager = multiprocessing.Manager()
        # lista = manager.list()

        # worker_thread = threading.Thread(target=worker_function, args=(app, quit_flag, lista))
        worker_thread = threading.Thread(target=worker_function, args=(app, quit_flag))
        worker_thread.start()

        # sync_thread = threading.Thread(target=sync_function, args=(app, quit_flag))
        # sync_thread.start()

        # color_thread = threading.Thread(target=feedbackColor, args=(quit_flag, lista))
        # color_thread.start()

        #logging.info("quit_flag.value = %s" % bool(quit_flag.value))
    except Exception as e:
        print('Prepare app thread: ', e)

    try:
        app.mainloop()
    except KeyboardInterrupt:
        #logging.info("Keyboard interrupt")
        print("Keyboard interrupt")
    quit_flag.value = True
    #logging.info("quit_flag.value = %s" % bool(quit_flag.value))
    worker_thread.join()
    # sync_thread.join()
    # color_thread.join()
#endregion
