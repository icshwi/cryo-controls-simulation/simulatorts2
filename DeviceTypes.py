
# ─── IMPORTS ────────────────────────────────────────────────────────────────────

from time import sleep
from opc_client import OPCClient
from myLib import *

# ─── DEVICE TYPES ───────────────────────────────────────────────────────────────

#region ─── NOT USED ───────────────────────────────────────────────────────────────────

class OnOffValve():
    """
    For on-off or PV valves.
    """

    def __init__(self, client, plcName, opened, closed, qOpen):
        self.client = client
        self.plcName = plcName
        self.qOpen = qOpen
        self.opened = opened
        self.closed = closed

    def SIM(self):
        '''
        Simulationg opening and closing function.
        '''
        valveOpened = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.opened])
        valveClosed = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.closed])
        valveOpen = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.qOpen])
        # Simulation
        if (OPCClient.getValue(self.client, valveOpen)) == True:
            OPCClient.setValue(self.client, valveClosed, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveOpened, True)
            return "Open"
        else:
            OPCClient.setValue(self.client, valveOpened, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveClosed, True)
            return "Close"

    def SIMopenedFault(self):
        '''
        Simulating open and open alarm.
        '''
        valveOpened = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.opened])
        valveClosed = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.opened])
        valveOpen = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.qOpen])
        # Simulation
        if (OPCClient.getValue(self.client, valveOpen)) == True:
            OPCClient.setValue(self.client, valveClosed, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveOpened, True)
            return "Open"
        else:
            OPCClient.setValue(self.client, valveOpened, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveClosed, True)
            return "Close"

    def SIMclosedFault(self):
        '''
        Simulating closed and closed alarm.
        '''
        valveOpened = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.closed])
        valveClosed = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.closed])
        valveOpen = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.qOpen])
        # Simulation
        if (OPCClient.getValue(self.client, valveOpen)) == True:
            OPCClient.setValue(self.client, valveClosed, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveOpened, True)
            return "Open"
        else:
            OPCClient.setValue(self.client, valveOpened, False)
            #sleep(0.001)
            OPCClient.setValue(self.client, valveClosed, True)
            return "Close"

# ────────────────────────────────────────────────────────────────────────────────

class DiscretePump():

    def __init__(self, client, plcName, valuePath):
        self.client = client
        self.plcName = plcName
        self.valuePath = valuePath
      
    def SIM(self):
        valueNode = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Outputs', self.valuePath])
        
        if (OPCClient.getValue(self.client, valueNode) == (True or 1)):
            return True
        else:
            return False

# ────────────────────────────────────────────────────────────────────────────────

class Heater():

    def __init__(self, client, plcName, solistate, contactor, contactorStatus, trip):
        self.client = client
        self.plcName = plcName
        self.solistate = solistate
        self.contactor = contactor
        self.contactorStatus = contactorStatus
        self.trip = trip

    def SIM(self):
        heaterSSt = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:Outputs', self.solistate])
        heaterCont = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:Outputs', self.contactor])
        heaterContStat = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:Inputs', self.contactorStatus])
        heaterTrip = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:Inputs', self.trip])

        if (OPCClient.getValue(self.client, heaterCont)) == True:
            OPCClient.setValue(self.client, heaterContStat, True)
        else:
            OPCClient.setValue(self.client, heaterContStat, False)
        
        noTrip = True
        if noTrip:
            OPCClient.setValue(self.client, heaterTrip, False)
        else:
            OPCClient.setValue(self.client, heaterTrip, True)

        if (OPCClient.getValue(self.client, heaterSSt)) == True:
            return "ON"
        else:
            return "OFF"

# ────────────────────────────────────────────────────────────────────────────────

class SC():

    def __init__(self, client, plcName, freqPath, feedbackPath):
        self.client = client
        self.plcName = plcName
        self.freqPath = freqPath
        self.feedbackPath = feedbackPath

    def SIM(self):
        scFreq = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.freqPath])
        scFb = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, '3:Inputs', self.feedbackPath])
        # Simulation
        freq = OPCClient.getValue(self.client, scFreq)
        if (OPCClient.getValue(self.client, freq)) > 0:
            OPCClient.setValue(self.client, scFb, freq)
        else:
            OPCClient.setValue(self.client, scFb, 0)
        return scFb

# ────────────────────────────────────────────────────────────────────────────────

# #ACTIVATE SIMULATION
# cabtrsim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR1', '3:Inputs', '3:Simulation'])
# OPCClient.setValue(client, cabtrsim, True)
# valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:CABTR1', '3:Static', '3:CH1', '3:Tmes'])

def Cabtr(client, plcName, valuePath, tempValue):

    lsPath = valuePath.split('/')
    devID = lsPath[0]
    chanID = lsPath[1]
    
    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', devID, '3:Static', chanID, '3:Tmes'])

    if tempValue <= 400:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 400)
    if tempValue >= 0:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 0)
    
    return OPCClient.getValue(client, valueNode)

# ────────────────────────────────────────────────────────────────────────────────

# #ACTIVATE SIMULATION
# cernoxsim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82305', '3:Inputs', '3:Simulation'])
# OPCClient.setValue(client, cernoxsim, True)
# '0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82305', '3:Outputs', '3:TemperatureENG'

def Cernox(client, plcName, devID, tempValue):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', devID, '3:Outputs', '3:TemperatureENG'])

    if tempValue <= 400:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 400)
    if tempValue >= 0:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 0)
    
    return OPCClient.getValue(client, valueNode)

# ────────────────────────────────────────────────────────────────────────────────

# #ACTIVATE SIMULATION
# pt100sim = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82360', '3:Inputs', '3:Simulation'])
# OPCClient.setValue(client, pt100sim, True)
# '0:Objects', '3:TS2_LCS', '3:DataBlocksInstance', '3:TE_82360', '3:Outputs', '3:TemperatureENG'

def Pt100(client, plcName, devID, tempValue):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', devID, '3:Outputs', '3:TemperatureENG'])

    if tempValue <= 400:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 400)
    if tempValue >= 0:
        OPCClient.setValue(client, valueNode, tempValue)
    else:
        OPCClient.setValue(client, valueNode, 0)
    
    return OPCClient.getValue(client, valueNode)

# ────────────────────────────────────────────────────────────────────────────────

# IsoVac_OK = "TS2-010CRM:Vac-VGC-01100-D0"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-01100-D0'])
# Cav1Vac_OK = "TS2-010CRM:Vac-VGC-10000-D0"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-10000-D0'])
# Cav2Vac_OK = "TS2-010CRM:Vac-VGC-20000-D0"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-20000-D0'])
# Cav3Vac_OK = "TS2-010CRM:Vac-VGC-30000-D0"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-30000-D0'])
# Cav4Vac_OK = "TS2-010CRM:Vac-VGC-40000-D0"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-40000-D0'])

# CdsVac_OK = "TS2-010CDS:Cryo-PT-82370-OK"
# client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CDS:Cryo-PT-82370-OK'])

def Vacuum(client, name, boolVal):

    if name.casefold() == 'isovac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-01100-D0'])
    elif name.casefold() == 'cav1vac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-10000-D0'])
    elif name.casefold() == 'cav2vac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-20000-D0'])
    elif name.casefold() == 'cav3vac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-30000-D0'])
    elif name.casefold() == 'cav4vac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CRM:Vac-VGC-40000-D0'])
    elif name.casefold() == 'cdsvac':
        valueNode = client.get_root_node().get_child(['0:Objects', '3:TS2_LCS', '3:Inputs', '3:TS2-010CDS:Cryo-PT-82370-OK'])
    else:
        valueNode = None

    if valueNode != None:
        if boolVal == (True or 1):
            OPCClient.setValue(client, valueNode, True)
        else:
            OPCClient.setValue(client, valueNode, False)
        return OPCClient.getValue(client, valueNode)

    return None

#endregion

# ────────────────────────────────────────────────────────────────────────────────

def Digital(client, plcName, valuePath, boolVal):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:Inputs', valuePath])

    if valueNode != None:
        if boolVal == (True or 1):
            OPCClient.setValue(client, valueNode, True)
        else:
            OPCClient.setValue(client, valueNode, False)
        return OPCClient.getValue(client, valueNode)

    return None

# ────────────────────────────────────────────────────────────────────────────────

def Analog(client, plcName, valuePath, rawValue):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:Inputs', valuePath])

    if valueNode != None:
        if 0 < rawValue <= 27648:
            OPCClient.setValue(client, valueNode, rawValue)

        if rawValue <= 0:
            OPCClient.setValue(client, valueNode, 0)

        if rawValue > 27648:
            OPCClient.setValue(client, valueNode, 27648)        
        return OPCClient.getValue(client, valueNode)

    return None

# ────────────────────────────────────────────────────────────────────────────────

def Sipart(client, plcName, valuePath, rawValue):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:Inputs', valuePath])

    if valueNode != None:
        if 0 < rawValue <= 27648:
            OPCClient.setValue(client, valueNode, realToDWORD(rawValue))

        if rawValue <= 0:
            OPCClient.setValue(client, valueNode, realToDWORD(0))

        if rawValue > 27648:
            OPCClient.setValue(client, valueNode, realToDWORD(27648))            
        return DWORDtoReal(OPCClient.getValue(client, valueNode))

    return None

# ────────────────────────────────────────────────────────────────────────────────

def GetVal(client, plcName, valuePath):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:Inputs', valuePath])

    if valueNode != None:    
        return OPCClient.getValue(client, valueNode)

    return None

# ────────────────────────────────────────────────────────────────────────────────

def GetSipartVal(client, plcName, valuePath):

    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:Inputs', valuePath])
    
    if valueNode != None:
        return DWORDtoReal(OPCClient.getValue(client, valueNode))

    return None
    
# ────────────────────────────────────────────────────────────────────────────────

def GetCabtrVal(client, plcName, valuePath):

    lsPath = valuePath.split('/')
    devID = lsPath[0]
    chanID = lsPath[1]
    
    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', devID, '3:Static', chanID, '3:Tmes'])
    
    if valueNode != None:
        return OPCClient.getValue(client, valueNode)

    return None
    
# ────────────────────────────────────────────────────────────────────────────────

def GetCernoxVal(client, plcName, valuePath):
    
    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', valuePath, '3:Outputs', '3:TemperatureENG'])
    
    if valueNode != None:
        return OPCClient.getValue(client, valueNode)

    return None
    
# ────────────────────────────────────────────────────────────────────────────────

def GetPt100Val(client, plcName, valuePath):
    
    valueNode = client.get_root_node().get_child(['0:Objects', plcName, '3:DataBlocksInstance', valuePath, '3:Outputs', '3:TemperatureENG'])
    
    if valueNode != None:
        return OPCClient.getValue(client, valueNode)

    return None
    
# ────────────────────────────────────────────────────────────────────────────────