
from opc_client import OPCClient
import math, struct
from decimal import Decimal
import time

def calc(inp):
    try:       
        return eval(str(inp).strip())
    except:
        return 0
    # except Exception as e:
    #     print('hiba: ', e)

def scale(input, lo_lim=0, hi_lim=100, bipolar=False):
# OUT = [((FLOAT (IN) - K1)/(K2-K1)) * (HI_LIM-LO_LIM)] + LO_LIM
#   BIPOLAR:                                                                    UNIPOLAR:
#   K1 = -27648                                                                 K1 = 0
#   K2 = 27648                                                                  K2 = 27648
#   OUT = [((FLOAT (IN) + 27648)/(27648+27648)) * (HI_LIM-LO_LIM)] + LO_LIM     OUT = [((FLOAT (IN) - 0)/(27648-0)) * (HI_LIM-LO_LIM)] + LO_LIM
    try:
        input = float(input)
        lo_lim = float(lo_lim)
        hi_lim = float(hi_lim)

        if bipolar == False:
            K1 = float(0)
            K2 = float(27648)
        else:
            K1 = float(-27648)
            K2 = float(27648)

        val1 = input - K1
        val2 = K2 - K1
        val3 = hi_lim - lo_lim
        val4 = lo_lim
            
        if K1 <= input <= K2:
            return val1 / val2 * val3 + val4
        else:
            if K1 > input:
                return lo_lim
            if K2 < input:
                return hi_lim
    except:
        return 0                

def unscale(input, lo_lim=0, hi_lim=100, bipolar=False):
# OUT = [ ((IN-LO_LIM)/(HI_LIM-LO_LIM)) * (K2-K1) ] + K1
#   BIPOLAR:                                                            UNIPOLAR:
#   K1 = -27648                                                         K1 = 0
#   K2 = 27648                                                          K2 = 27648
#   OUT = [ ((IN-LO_LIM)/(HI_LIM-LO_LIM)) * (27648+27648) ] - 27648     OUT = [ ((IN-LO_LIM)/(HI_LIM-LO_LIM)) * (27648-0) ] + 0
    try:
        input = float(input)
        lo_lim = float(lo_lim)
        hi_lim = float(hi_lim)

        if bipolar == False:
            K1 = float(0)
            K2 = float(27648)
        else:
            K1 = float(-27648)
            K2 = float(27648)

        val1 = input - lo_lim   
        val2 = hi_lim - lo_lim
        if val2 == 0:
            val2 = 1
        val3 = K2 - K1
        val4 = K1

        if lo_lim <= input <= hi_lim:
            return val1 / val2 * val3 + val4
        else:
            if lo_lim > input:
                return K1
            if hi_lim < input:
                return K2
    except:
        return 0

def to_float(int_):
    return struct.unpack('d', struct.pack('q', int_))[0]

def to_long(float_):
    return struct.unpack('q', struct.pack('d', float_))[0]

#region ─── REAL TO DWORD ──────────────────────────────────────────────────────────────

# Python program to convert a real value
# to IEEE 754 Floating Point Representation. 
# Function to convert a
# fraction to binary form.
def binaryOfFraction(fraction):

    # Declaring an empty string
    # to store binary bits.
    binary = str()

    # Iterating through
    # fraction until it
    # becomes Zero.
    #fraction = round(fraction, 3)
    while (fraction):

        # Multiplying fraction by 2.
        fraction *= 2
    
        # Storing Integer Part of
        # Fraction in int_part.
        if (fraction >= 1):
            int_part = 1
            fraction -= 1
            #fraction = round(fraction, 3)
        else:
            int_part = 0

        # Adding int_part to binary
        # after every iteration.
        binary += str(int_part)
    
    # Returning the binary string.
    return binary
 
# Function to get sign  bit,
# exp bits and mantissa bits,
# from given real no.
def floatingPoint(real_no):
 
    # Function call to convert
    # Fraction part of real no
    # to Binary.
    #fraction_str = binaryOfFraction(real_no - int(real_no))
    fraction_str = decimalToBinary(to_long(real_no))[-52:]
 
    # Setting Sign bit
    # default to zero.
    sign_bit = 0
 
    # Sign bit will set to
    # 1 for negative no.
    if(real_no < 0):
        sign_bit = 1
 
    # converting given no. to
    # absolute value as we have
    # already set the sign bit.
    real_no = abs(real_no)
 
    # Converting Integer Part
    # of Real no to Binary
    int_str = bin(int(real_no))[2 : ]
 
    # Getting the index where
    # Bit was high for the first
    # Time in binary repres
    # of Integer part of real no.   
    if '1' in int_str:
        ind = int_str.index('1')
    else:
        ind = 0

    # The Exponent is the no.
    # By which we have right
    # Shifted the decimal and
    # it is given below.
    # Also converting it to bias
    # exp by adding 127.

    if 0.0 <= real_no < 0.1:
        exp_str = bin((len(int_str) - ind - 1) + 0)[2 : ]
    if 0.1 <= real_no < 0.2:
        exp_str = bin((len(int_str) - ind - 1) + 123)[2 : ]
    if 0.2 <= real_no < 0.3:
        exp_str = bin((len(int_str) - ind - 1) + 124)[2 : ]
    if 0.3 <= real_no < 0.5:
        exp_str = bin((len(int_str) - ind - 1) + 125)[2 : ]
    if 0.5 <= real_no < 1.0:
        exp_str = bin((len(int_str) - ind - 1) + 126)[2 : ]
    if 1.0 <= real_no:
        exp_str = bin((len(int_str) - ind - 1) + 127)[2 : ]
    
    exp_str = ('0' * (8 - len(exp_str))) + exp_str

    # getting mantissa string
    # By adding int_str and fraction_str.
    # the zeroes in MSB of int_str
    # have no significance so they
    # are ignored by slicing.
    #mant_str = int_str[ind + 1 : ] + fraction_str

    # Adding Zeroes in LSB of
    # mantissa string so as to make
    # it's length of 23 bits.
    mant_str = fraction_str
    mant_str = mant_str + ('0' * (23 - len(mant_str)))
    
    # Returning the sign, Exp
    # and Mantissa Bit strings.    
    return sign_bit, exp_str, mant_str

# Function to convert Binary number
# to Decimal number
def binaryToDecimal(n):
    return int(n,2)

def realToDWORD(n):
    sign_bit, exp_str, mant_str = floatingPoint(n)
 
    # Final Floating point Representation.
    ieee_32 = str(sign_bit) + exp_str + mant_str

    return binaryToDecimal(ieee_32[:32])
#endregion

#region ─── DWORD TO REAL ──────────────────────────────────────────────────────────────

# Python program to convert
# IEEE 754 floating point representation
# into real value
 
# Function to convert Binary
# of Mantissa to float value.
def convertToInt(mantissa_str):
 
    # variable to make a count
    # of negative power of 2.
    power_count = -1
 
    # variable to store
    # float value of mantissa.
    mantissa_int = 0
 
    # Iterations through binary
    # Number. Standard form of
    # Mantissa is 1.M so we have
    # 0.M therefore we are taking
    # negative powers on 2 for
    # conversion.
    for i in mantissa_str:
 
        # Adding converted value of
        # Binary bits in every
        # iteration to float mantissa.
        mantissa_int += (int(i) * pow(2, power_count))
 
        # count will decrease by 1
        # as we move toward right.
        power_count -= 1
         
    # returning mantissa in 1.M form.
    return (mantissa_int + 1)

# Function to convert Decimal number
# to Binary number 
def decimalToBinary(n):
    return bin(n).replace("0b","0")

def DWORDtoReal(n):
 
    ieee_32 = decimalToBinary(n)
    
    ieee_32 = ('0' * (32 - len(ieee_32))) + ieee_32
    
    if int(ieee_32) == 0:
        return 0.0

    # First bit will be sign bit.
    sign_bit = int(ieee_32[0])
 
    # Next 8 bits will be
    # Exponent Bits in Biased
    # form.    
    exponent_bias = int(ieee_32[1 : 9], 2)
 
    # In 32 Bit format bias
    # value is 127 so to have
    # unbiased exponent
    # subtract 127.
    exponent_unbias = exponent_bias - 127
 
    # Next 23 Bits will be
    # Mantissa (1.M format)
    mantissa_str = ieee_32[9 : ]
 
    # Function call to convert
    # 23 binary bits into
    # 1.M real no. form
    mantissa_int = convertToInt(mantissa_str)
 
    # The final real no. obtained
    # by sign bit, mantissa and
    # Exponent.
    real_no = pow(-1, sign_bit) * mantissa_int * pow(2, exponent_unbias)
 
    # Printing the obtained
    # Real value of floating
    # Point Representation.

    return(real_no)
#endregion

def limit(input, min, max):
    try:
        input = float(input)
        min = float(min)
        max = float(max)

        if min < input < max:
            return input
        if input <= 0:
            return min
        if input >= max:
            return max
    except:
        return 0

class readDBvalue():
    def __init__(self, client, plcName, dbPath, valuePath):
      self.client = client
      self.plcName = plcName
      self.dbPath = dbPath
      self.valuePath = valuePath
    
    def read(self):
        #dbValue = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:DataBlocksInstance', self.dbPath, '3:Inputs', self.valuePath])
        value = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, 'DataBlockInstance', self.dbPath, '3:Inputs', self.valuePath])

        valueFromDB = OPCClient.getValue(self.client, value)
        return valueFromDB