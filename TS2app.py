from crypt import methods
from doctest import master
from email.mime import application
from mimetypes import init
from multiprocessing.connection import Client
from tkinter import *
from tkinter import filedialog
import tkinter
from tkinter import ttk
#from turtle import bgcolor, ondrag
from types import FrameType
import pygubu
import multiprocessing
import threading
import time
import logging
from time import sleep
from datetime import datetime

from pygubu import builder
from opc_client import OPCClient
from DeviceTypes import *
from myLib import *
from epics import caget
import numpy as np
import random
import inspect
import re
import sys
import os

#from test_threading import print_square

class Application(Frame):

#region ─── INIT ───────────────────────────────────────────────────────────────────────
    def __init__(self, master=None):

        # with open('TS2_SIM_v2.ui', "r+") as f:
        #     data = f.read()
        #     f.seek(0)
        #     f.write(re.sub('1680', '500', data))
        #     f.truncate()
        #     f.close()

        try:
            Frame.__init__(self, master)
            self.poll() 
            self.builder = builder = pygubu.Builder()
            builder.add_from_file('TS2_SIM_v2.ui')
            self.mainwindow = builder.get_object('mainwindow', master)
            builder.connect_callbacks(self)
        except Exception as e:
            print('Prepare main window: ', e)

        # self.baseframe = self.builder.get_object('baseframe')
        # self.xscrollbar = self.builder.get_object('xscrollbar')

        # #self.baseframe.config(xscrollcommand = self.xscrollbar.set)
        # #self.xscrollbar.config(command = self.baseframe.xview)

        # self.baseframe['xscrollcommand'] = self.xscrollbar.set
        # self.xscrollbar['command'] = self.baseframe.xview

        self.FirstRun = True
        self.InitReady = False
        self.ValuesFromPLC = False

        self.actState = 0
        self.actSubState = 0
        self.actSubSubState = 0
        self.actStateName = ""
        self.actSubStateName = ""
        self.actSubSubStateName = ""
        #self.maxval = 32767
        self.maxval = 100
        self.minval = 1

        self.names = []
        self.types = []
        self.paths = []
        self.states = []
        self.entrys = []
        self.saventrys = []
        self.scmins = []
        self.lolims = []
        self.hilims = []
        self.scmaxs = []
        lines = []
        self.saves = []
        self.savedval = []

        try:           
            if sys.argv[2].casefold() == 'true' or sys.argv[2] == '1':
                self.ValuesFromPLC = True
            else:
                self.ValuesFromPLC = False
            
            switch = False
            with open(sys.argv[1], "r") as f:
                #data = f.read()
                #lines = f.readlines()
                for line in f:
                    if line == '':
                        break
                    if line == '\n':
                        switch = True
                        continue
                    else:
                        if switch == False:
                            lines.append(line)
                        else:                    
                            self.saves.append(line)
                f.close()

        except Exception as e:
            print('Open file: ', e)

        emptyfirst = [None] * 20
        empty = [None] * 9
        first = False

        for line in lines:
            items = list(line.split(','))
            if first == False:
                self.names.extend(emptyfirst)
                self.types.extend(emptyfirst)
                self.paths.extend(emptyfirst)
                self.states.extend(emptyfirst)
                first = True
            else:
                self.names.extend(empty)
                self.types.extend(empty)
                self.paths.extend(empty)
                self.states.extend(empty)
            self.names.append(items.pop(0))
            self.types.append(items.pop(0))
            self.paths.append(items.pop(0))
            self.states.append(items)

        # print(*self.names)
        # print(*self.types)
        # print(*self.paths)
        # print(*self.states)

        self.entrys.extend(self.names)
        self.saventrys.extend(self.names)
        self.scmins.extend(self.names)
        self.lolims.extend(self.names)
        self.hilims.extend(self.names)
        self.scmaxs.extend(self.names)
        self.savedval.extend(self.names)
        self.savedval = np.full(len(self.savedval),True)

        for name in self.names:
            if name != None:
                index = self.names.index(name)
                exist = self.checkItem('frame'+str(index))
                if exist:
                    entry = self.getSavedVal(index, 'entrys').strip()
                    scmin = self.getSavedVal(index, 'scmins').strip()
                    lolim = self.getSavedVal(index, 'lolims').strip()
                    hilim = self.getSavedVal(index, 'hilims').strip()
                    scmax = self.getSavedVal(index, 'scmaxs').strip()
                    
                    self.setLabel('label'+str(index), self.names[index]) 
                    self.setEntry('scminvar'+str(index), scmin)
                    self.setEntry('lolimvar'+str(index), lolim)
                    self.setEntry('hilimvar'+str(index), hilim)
                    self.setEntry('scmaxvar'+str(index), scmax)
                    self.entrys[index] = float(entry)
                    self.saventrys[index] = float(entry)
                    self.scmins[index] = int(scmin)
                    self.lolims[index] = int(lolim)
                    self.hilims[index] = int(hilim)
                    self.scmaxs[index] = int(scmax)

                    #self.entry(index)
                    self.entrymin(index, index)
                    self.entrylow(index, index)
                    self.entryhigh(index, index)
                    self.entrymax(index, index)

        with open('TS2_SIM_v2.ui', "r+") as f:
            data = f.read()
            nums = re.findall(r'frame(\d+)', data)
            maxframe = int(nums[-1])
            #print(maxframe)
            f.close()

        for rest in range(len(self.names) + 9, maxframe + 1, 10):
            self.hideFrame('frame'+str(rest))

        if len(self.names) < 1280:
            self.hideFrame('frame07')

        if len(self.names) < 1100:
            self.hideFrame('frame06')

        if len(self.names) < 920:
            self.hideFrame('frame05')

        if len(self.names) < 740:
            self.hideFrame('frame04')

        if len(self.names) < 560:
            self.hideFrame('frame03')

        if len(self.names) < 380:
            self.hideFrame('frame02')

        if len(self.names) < 200:
            self.hideFrame('frame01')

        if len(self.names) < 20:
            self.hideFrame('frame00')

#endregion

#region ─── POLL ───────────────────────────────────────────────────────────────────────
    def poll(self):
        """
        This method is required to allow the mainloop to receive keyboard
        interrupts when the frame does not have the focus
        """
        self.master.after(250, self.poll)
#endregion

#region ─── BINDINGS ───────────────────────────────────────────────────────────────────
    def on_sync_button_click (self):
        self.FirstRun = True
        self.InitReady = False

        # print(self.actStateName)
        # print(self.actSubStateName)
        # print(self.actSubSubStateName)

    def on_saveas_button_click (self):       
        
        flname = filedialog.asksaveasfilename()

        if flname != '':
            
            self.saves.clear()

            co = ','
            for name in self.names:
                if name != None:
                    index = self.names.index(name)
                    s1 = str(index)
                    s2 = str(self.entrys[index])
                    s3 = str(self.scmins[index])
                    s4 = str(self.lolims[index])
                    s5 = str(self.hilims[index])
                    s6 = str(self.scmaxs[index])
                    self.saves.append(s1+co+s2+co+s3+co+s4+co+s5+co+s6)

            try:
                switch = False
                with open(sys.argv[1], "r") as f:
                    lines = f.readlines()
                    f.close()

                # flname = os.path.splitext(sys.argv[1])[0]
                # flname = ''.join([i for i in flname if i.isalpha()])
                # sys.argv[1] = flname + str(datetime.now().strftime("_%Y%m%d_%H%M")) + ".txt"

                sys.argv[1] = flname

                with open(sys.argv[1], "w") as f:
                    for line in lines:
                        if line == '\n' or line == '':
                            f.write('\n')
                            for save in self.saves:
                                f.write(save + '\n')
                            break
                        else:
                            f.write(line)
                    f.close()
                
                self.savedval = np.full(len(self.savedval),True)

            except Exception as e:
                print('Save as to file: ', e)

    def on_save_button_click (self):
        self.saves.clear()

        co = ','
        for name in self.names:
            if name != None:
                index = self.names.index(name)
                s1 = str(index)
                s2 = str(self.entrys[index])
                s3 = str(self.scmins[index])
                s4 = str(self.lolims[index])
                s5 = str(self.hilims[index])
                s6 = str(self.scmaxs[index])
                self.saves.append(s1+co+s2+co+s3+co+s4+co+s5+co+s6)

        try:
            switch = False
            with open(sys.argv[1], "r") as f:
                lines = f.readlines()
                f.close()
            with open(sys.argv[1], "w") as f:
                for line in lines:
                    if line == '\n' or line == '':
                        f.write('\n')
                        for save in self.saves:
                            f.write(save + '\n')
                        break
                    else:
                        f.write(line)
                f.close()
            
            self.savedval = np.full(len(self.savedval),True)

        except Exception as e:
            print('Save to file: ', e)
    
    def on_quit_button_click (self):
        self.master.quit()

        # self.info(unscale(self.entrys[20], self.scmins[20], self.scmaxs[20]))
        # self.info(self.entrys[20])
        # self.info(self.scmins[20])
        # self.info(self.scmaxs[20])

        # lista = self.getallChild('baseframe')
        # print(*lista, '\n')

        # fr = self.builder.get_object('frame01')
        # fr.pack_forget()

        # print(self.actState)
        # print(self.actSubState)
        # print(self.actSubSubState)

        # print('Names : ', *self.getListVal(self.names), '\n')
        # print('NamesID : ',*self.getListValID(self.names), '\n')
        # print('Entrys : ',*self.getListVal(self.entrys), '\n')
        # print('EntrysID : ',*self.getListValID(self.entrys), '\n')
        # print('ScMins : ',*self.getListVal(self.scmins), '\n')
        # print('ScMinsID : ',*self.getListValID(self.scmins), '\n')
        # print('LoLims : ',*self.getListVal(self.lolims), '\n')
        # print('LoLimsID : ',*self.getListValID(self.lolims), '\n')
        # print('HiLims : ',*self.getListVal(self.hilims), '\n')
        # print('HiLimsID : ',*self.getListValID(self.hilims), '\n')
        # print('ScMaxs : ',*self.getListVal(self.scmaxs), '\n')
        # print('ScMaxsID : ',*self.getListValID(self.scmaxs), '\n')

#endregion

#region ─── METHODS ────────────────────────────────────────────────────────────────────
    def getSavedVal(self, index, name):
        if name == 'entrys':
            id = 1
        elif name == 'scmins':
            id = 2
        elif name == 'lolims':
            id = 3
        elif name == 'hilims':
            id = 4
        elif name == 'scmaxs':
            id = 5
        else:
            id = 0
        
        for save in self.saves:
            if save.startswith(str(index)):
                items = list(save.split(','))
                return items[id]
    
    def setBusyCursor(self):
        item = self.builder.get_object('mainwindow')
        item.config(cursor='watch')

    def setDefCursor(self):
        item = self.builder.get_object('mainwindow')
        item.config(cursor='')
    
    def info(self, name):
        print('name: ', name)
        print('type: ', type(name))
    
    def getallChild(self, name):
        fr = self.builder.get_object(name)
        return [fr.winfo_children()]

    def hideFrame(self, name):
        fr = self.builder.get_object(name)
        fr.pack_forget()

    def checkItem(self, name):
        try:
            self.builder.get_object(name)
            return True
        except:
            return False
    
    def getListVal(self, items):
        retlist = []
        for item in items:
            if str(item) != 'None' and str(item) != '':
                retlist.append(str(item))
        return retlist

    def getListValID(self, items):
        retlist = []
        for item in items:
            if str(item) != 'None' and str(item) != '':
                retlist.append(items.index(item))
        return retlist

    def stateMachine(self, state):
        st = state.replace(' ', '0')
        numbers = re.findall(r'\d+', st)
        number = numbers[0]
        return int(number[0:3]), int(number[3:6]), int(number[6:9])

    def setLabel(self, name, txt):
        label = self.builder.get_object(name)
        label.config(text=txt)

    def setEntry(self, varname, txt):
        var = self.builder.get_variable(varname)
        var.set(txt)

    def getEntry(self, entry):
        return self.builder.get_object(entry).get()

    def setBorder(self, name, border):
        item = self.builder.get_object(name)
        item.config(bd=border)

    def getBorder(self, name):
        item = self.builder.get_object(name)
        #return item['bd']
        return item.cget('bd')

    def setColor(self, name, color):
        item = self.builder.get_object(name)
        item.config(bg=color)

    def getColor(self, name):
        item = self.builder.get_object(name)
        #return item['bg']
        return item.cget('bg')

    def setScale(self, name, value):
        self.builder.get_object(name).set(value)

    def getScale(self, name):
        return self.builder.get_object(name).get()

    def setScaleMin(self, name, min):
        scale = self.builder.get_object(name)
        scale.configure(from_=min)

    def setScaleMax(self, name, max):
        scale = self.builder.get_object(name)
        scale.configure(to=max) 

    def getScaleMin(self, name):
        scale = self.builder.get_object(name)
        #return scale['from']
        return scale.cget('from')

    def getScaleMax(self, name):
        scale = self.builder.get_object(name)
        #return scale['to']
        return scale.cget('to')

    def incrementScale(self, name, increment):
        var = self.builder.get_object(name).get()
        var += float(increment)
        self.builder.get_object(name).set(var)
    
    def entry(self, num):
        try:    
            var = self.getEntry('entry' + str(num))
            var = calc(var)
            self.setScale('scale' + str(num), var)
            #self.entrys[index] = var
        except Exception as e:
            print('entry: ', e)

    def setEntryScale(self, num, value):
        try:    
            self.setEntry('entryvar' + str(num), value)
            self.setScale('scale' + str(num), value)
            #self.entrys[index] = value
        except Exception as e:
            print('entryscale: ', e)

    def incscale(self, num, inc):
        try:
            self.incrementScale('scale' + str(num), inc)
            var = self.getScale('scale' + str(num))
            self.setEntry('entryvar' + str(num), var)
            #self.entrys[index] = var
        except Exception as e:
            print('incscale: ', e)

    def scale(self, num, index):
        try:
            var = self.getScale('scale' + str(num))
            self.setEntry('entryvar' + str(num), var)
            if self.InitReady == True and self.entrys[index] != var:
                self.savedval[index] = False
            self.entrys[index] = var    # new entrys call this event -> actual entry is enough to save here!!!
            focus = str(self.focus_get())
            frame = 'frame' + str(int(num/10))
            if frame not in focus:
                self.setFocus('entry' + str(num))
        except Exception as e:
            print('scale: ', e)

    def entrymin(self, num, index):
        try:    
            var = self.getEntry('scmin' + str(num))
            var = calc(var)
            if self.InitReady == True and self.scmins[index] != var:
                self.savedval[index] = False            
            self.scmins[index] = var
            self.setScaleMin('scale' + str(num), var)
        except Exception as e:
            print('entrymin: ', e)

    def entrylow(self, num, index):
        try:    
            var = self.getEntry('lolim' + str(num))
            var = calc(var) 
            if self.InitReady == True and self.lolims[index] != var:
                self.savedval[index] = False            
            self.lolims[index] = var
        except Exception as e:
            print('entrylow: ', e) 

    def entryhigh(self, num, index):
        try:    
            var = self.getEntry('hilim' + str(num))
            var = calc(var) 
            if self.InitReady == True and self.hilims[index] != var:
                self.savedval[index] = False            
            self.hilims[index] = var
        except Exception as e:
            print('entryhigh: ', e) 

    def entrymax(self, num, index):
        try:    
            var = self.getEntry('scmax' + str(num))
            var = calc(var)
            if self.InitReady == True and self.scmaxs[index] != var:
                self.savedval[index] = False
            self.scmaxs[index] = var
            self.setScaleMax('scale' + str(num), var)
        except Exception as e:
            print('entrymax: ', e)

    def getMethodID(self):
        ID = re.findall(r'\d+', inspect.stack()[1][3])
        return int(ID[0])
        #print(inspect.stack()[0][3])
        #print(inspect.stack()[1][3])

    def setFocus(self, name):
        obj = self.builder.get_object(name)
        obj.focus_set()

    def getNames(self):
        return self.names      

    def getTypes(self):
        return self.types      

    def getStates(self):
        return self.states

    def getEntrys(self):
        return self.entrys

    def getScmins(self):
        return self.scmins

    def getLolims(self):
        return self.lolims

    def getHilims(self):
        return self.hilims

    def getScmaxs(self):
        return self.scmaxs                                

#endregion

#region ─── ENTRYID ────────────────────────────────────────────────────────────────────
    def focusout_entryid(self, event=None):
        pass
        # try:
        #     var = self.getEntry('entryid')
        #     var = calc(var)
        #     self.actState = var
        # except Exception as e:
        #     print('focusout_entryid: ', e)

    def down_entryid(self, event=None):
        try:    
            var = self.getEntry('entryid')
            var = calc(var) - 10
            if var >= self.minval:
                self.actState = var
                self.setEntry('entryvarid', var)
            else:
                self.setEntry('entryvarid', self.minval)
        except Exception as e:
            print('down_entryid: ', e)

    def entered_entryid(self, event=None):
        try:    
            var = self.getEntry('entryid')
            var = calc(var)
            if var >= self.minval and var <= self.maxval:
                self.actState = var
            else:
                if var < self.minval:
                    self.setEntry('entryvarid', self.minval)
                if var > self.maxval:
                    self.setEntry('entryvarid', self.maxval)
        except Exception as e:
            print('entered_entryid: ', e)

    def left_entryid(self, event=None):
        try:    
            var = self.getEntry('entryid')
            var = calc(var) - 1
            if var >= self.minval:
                self.actState = var
                self.setEntry('entryvarid', var)
            else:
                self.setEntry('entryvarid', self.minval) 
        except Exception as e:
            print('left_entryid: ', e)

    def right_entryid(self, event=None):
        try:    
            var = self.getEntry('entryid')
            var = calc(var) + 1
            if var <= self.maxval:
                self.actState = var
                self.setEntry('entryvarid', var)
            else:
                self.setEntry('entryvarid', self.maxval) 
        except Exception as e:
            print('right_entryid: ', e)

    def up_entryid(self, event=None):
        try:    
            var = self.getEntry('entryid')
            var = calc(var) + 10
            if var <= self.maxval:
                self.actState = var
                self.setEntry('entryvarid', var)
            else:
                self.setEntry('entryvarid', self.maxval) 
        except Exception as e:
            print('up_entryid: ', e)
#endregion

#region ─── ENTRYSUBID ─────────────────────────────────────────────────────────────────
    def focusout_entrysubid(self, event=None):
        pass
        # try:
        #     var = self.getEntry('entrysubid')
        #     var = calc(var)
        #     self.actSubState = var
        # except Exception as e:
        #     print('focusout_entrysubid: ', e)

    def down_entrysubid(self, event=None):
        try:
            var = self.getEntry('entrysubid')
            var = calc(var) - 10
            if var >= self.minval:
                self.actSubState = var
                self.setEntry('entryvarsubid', var)
            else:
                self.setEntry('entryvarsubid', self.minval) 
        except Exception as e:
            print('down_entrysubid: ', e)

    def entered_entrysubid(self, event=None):
        try:
            var = self.getEntry('entrysubid')
            var = calc(var)
            if var >= self.minval and var <= self.maxval:
                self.actSubState = var
            else:
                if var < self.minval:
                    self.setEntry('entryvarsubid', self.minval)
                if var > self.maxval:
                    self.setEntry('entryvarsubid', self.maxval) 
        except Exception as e:
            print('entered_entrysubid: ', e)

    def left_entrysubid(self, event=None):
        try:
            var = self.getEntry('entrysubid')
            var = calc(var) - 1
            if var >= self.minval:
                self.actSubState = var
                self.setEntry('entryvarsubid', var)
            else:
                self.setEntry('entryvarsubid', self.minval) 
        except Exception as e:
            print('left_entrysubid: ', e)

    def right_entrysubid(self, event=None):
        try:
            var = self.getEntry('entrysubid')
            var = calc(var) + 1
            if var <= self.maxval:
                self.actSubState = var
                self.setEntry('entryvarsubid', var)
            else:
                self.setEntry('entryvarsubid', self.maxval)
        except Exception as e:
            print('right_entrysubid: ', e)

    def up_entrysubid(self, event=None):
        try:
            var = self.getEntry('entrysubid')
            var = calc(var) + 10
            if var <= self.maxval:
                self.actSubState = var
                self.setEntry('entryvarsubid', var)
            else:
                self.setEntry('entryvarsubid', self.maxval)
        except Exception as e:
            print('up_entrysubid: ', e)
#endregion

#region ─── ENTRYSUBSUBID ──────────────────────────────────────────────────────────────
    def focusout_entrysubsubid(self, event=None):
        pass
        # try:
        #     var = self.getEntry('entrysubsubid')
        #     var = calc(var)
        #     self.actSubSubState = var
        # except Exception as e:
        #     print('focusout_entrysubsubid: ', e)

    def down_entrysubsubid(self, event=None):
        try:
            var = self.getEntry('entrysubsubid')
            var = calc(var) - 10
            if var >= self.minval:
                self.actSubSubState = var
                self.setEntry('entryvarsubsubid', var)
            else:
                self.setEntry('entryvarsubsubid', self.minval)
        except Exception as e:
            print('down_entrysubsubid: ', e)

    def entered_entrysubsubid(self, event=None):
        try:
            var = self.getEntry('entrysubsubid')
            var = calc(var)
            if var >= self.minval and var <= self.maxval:
                self.actSubSubState = var
            else:
                if var < self.minval:
                    self.setEntry('entryvarsubsubid', self.minval)
                if var > self.maxval:
                    self.setEntry('entryvarsubsubid', self.maxval) 
        except Exception as e:
            print('entered_entrysubsubid: ', e)

    def left_entrysubsubid(self, event=None):
        try:
            var = self.getEntry('entrysubsubid')
            var = calc(var) - 1
            if var >= self.minval:
                self.actSubSubState = var
                self.setEntry('entryvarsubsubid', var)
            else:
                self.setEntry('entryvarsubsubid', self.minval) 
        except Exception as e:
            print('left_entrysubsubid: ', e)

    def right_entrysubsubid(self, event=None):
        try:
            var = self.getEntry('entrysubsubid')
            var = calc(var) + 1
            if var <= self.maxval:
                self.actSubSubState = var
                self.setEntry('entryvarsubsubid', var)
            else:
                self.setEntry('entryvarsubsubid', self.maxval)
        except Exception as e:
            print('right_entrysubsubid: ', e)

    def up_entrysubsubid(self, event=None):
        try:
            var = self.getEntry('entrysubsubid')
            var = calc(var) + 10
            if var <= self.maxval:
                self.actSubSubState = var
                self.setEntry('entryvarsubsubid', var)
            else:
                self.setEntry('entryvarsubsubid', self.maxval)
        except Exception as e:
            print('up_entrysubsubid: ', e)
#endregion

#region ─── ROWS ───────────────────────────────────────────────────────────────────────

#region ─── Row_20 ──────────────────────────────────────────────────────────────────

    def focusout_entry20(self, event=None):
        self.entry(self.getMethodID())

    def down_entry20(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry20(self, event=None):
        self.entry(self.getMethodID())

    def left_entry20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry20(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin20(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin20(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin20(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin20(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim20(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim20(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim20(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim20(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale20(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim20(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim20(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim20(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim20(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax20(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax20(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax20(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax20(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax20(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax20(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_30 ──────────────────────────────────────────────────────────────────

    def focusout_entry30(self, event=None):
        self.entry(self.getMethodID())

    def down_entry30(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry30(self, event=None):
        self.entry(self.getMethodID())

    def left_entry30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry30(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin30(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin30(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin30(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin30(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim30(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim30(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim30(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim30(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale30(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim30(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim30(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim30(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim30(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax30(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax30(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax30(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax30(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax30(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax30(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_40 ──────────────────────────────────────────────────────────────────

    def focusout_entry40(self, event=None):
        self.entry(self.getMethodID())

    def down_entry40(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry40(self, event=None):
        self.entry(self.getMethodID())

    def left_entry40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry40(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin40(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin40(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin40(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin40(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim40(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim40(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim40(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim40(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale40(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim40(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim40(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim40(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim40(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax40(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax40(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax40(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax40(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax40(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax40(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_50 ──────────────────────────────────────────────────────────────────

    def focusout_entry50(self, event=None):
        self.entry(self.getMethodID())

    def down_entry50(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry50(self, event=None):
        self.entry(self.getMethodID())

    def left_entry50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry50(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin50(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin50(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin50(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin50(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim50(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim50(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim50(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim50(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale50(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim50(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim50(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim50(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim50(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax50(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax50(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax50(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax50(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax50(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax50(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_60 ──────────────────────────────────────────────────────────────────

    def focusout_entry60(self, event=None):
        self.entry(self.getMethodID())

    def down_entry60(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry60(self, event=None):
        self.entry(self.getMethodID())

    def left_entry60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry60(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin60(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin60(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin60(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin60(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim60(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim60(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim60(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim60(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale60(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim60(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim60(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim60(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim60(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax60(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax60(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax60(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax60(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax60(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax60(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_70 ──────────────────────────────────────────────────────────────────

    def focusout_entry70(self, event=None):
        self.entry(self.getMethodID())

    def down_entry70(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry70(self, event=None):
        self.entry(self.getMethodID())

    def left_entry70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry70(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin70(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin70(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin70(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin70(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim70(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim70(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim70(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim70(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale70(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim70(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim70(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim70(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim70(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax70(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax70(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax70(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax70(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax70(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax70(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_80 ──────────────────────────────────────────────────────────────────

    def focusout_entry80(self, event=None):
        self.entry(self.getMethodID())

    def down_entry80(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry80(self, event=None):
        self.entry(self.getMethodID())

    def left_entry80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry80(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin80(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin80(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin80(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin80(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim80(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim80(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim80(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim80(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale80(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim80(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim80(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim80(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim80(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax80(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax80(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax80(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax80(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax80(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax80(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_90 ──────────────────────────────────────────────────────────────────

    def focusout_entry90(self, event=None):
        self.entry(self.getMethodID())

    def down_entry90(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry90(self, event=None):
        self.entry(self.getMethodID())

    def left_entry90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry90(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin90(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin90(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin90(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin90(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim90(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim90(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim90(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim90(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale90(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim90(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim90(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim90(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim90(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax90(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax90(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax90(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax90(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax90(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax90(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_100 ──────────────────────────────────────────────────────────────────

    def focusout_entry100(self, event=None):
        self.entry(self.getMethodID())

    def down_entry100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry100(self, event=None):
        self.entry(self.getMethodID())

    def left_entry100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin100(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin100(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim100(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim100(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale100(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim100(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim100(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax100(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax100(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax100(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_110 ──────────────────────────────────────────────────────────────────

    def focusout_entry110(self, event=None):
        self.entry(self.getMethodID())

    def down_entry110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry110(self, event=None):
        self.entry(self.getMethodID())

    def left_entry110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin110(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin110(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim110(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim110(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale110(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim110(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim110(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax110(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax110(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax110(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_120 ──────────────────────────────────────────────────────────────────

    def focusout_entry120(self, event=None):
        self.entry(self.getMethodID())

    def down_entry120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry120(self, event=None):
        self.entry(self.getMethodID())

    def left_entry120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin120(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin120(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim120(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim120(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale120(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim120(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim120(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax120(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax120(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax120(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_130 ──────────────────────────────────────────────────────────────────

    def focusout_entry130(self, event=None):
        self.entry(self.getMethodID())

    def down_entry130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry130(self, event=None):
        self.entry(self.getMethodID())

    def left_entry130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin130(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin130(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim130(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim130(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale130(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim130(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim130(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax130(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax130(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax130(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_140 ──────────────────────────────────────────────────────────────────

    def focusout_entry140(self, event=None):
        self.entry(self.getMethodID())

    def down_entry140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry140(self, event=None):
        self.entry(self.getMethodID())

    def left_entry140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin140(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin140(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim140(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim140(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale140(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim140(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim140(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax140(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax140(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax140(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_150 ──────────────────────────────────────────────────────────────────

    def focusout_entry150(self, event=None):
        self.entry(self.getMethodID())

    def down_entry150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry150(self, event=None):
        self.entry(self.getMethodID())

    def left_entry150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin150(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin150(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim150(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim150(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale150(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim150(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim150(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax150(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax150(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax150(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_160 ──────────────────────────────────────────────────────────────────

    def focusout_entry160(self, event=None):
        self.entry(self.getMethodID())

    def down_entry160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry160(self, event=None):
        self.entry(self.getMethodID())

    def left_entry160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin160(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin160(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim160(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim160(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale160(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim160(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim160(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax160(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax160(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax160(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_170 ──────────────────────────────────────────────────────────────────

    def focusout_entry170(self, event=None):
        self.entry(self.getMethodID())

    def down_entry170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry170(self, event=None):
        self.entry(self.getMethodID())

    def left_entry170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin170(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin170(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim170(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim170(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale170(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim170(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim170(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax170(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax170(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax170(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_180 ──────────────────────────────────────────────────────────────────

    def focusout_entry180(self, event=None):
        self.entry(self.getMethodID())

    def down_entry180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry180(self, event=None):
        self.entry(self.getMethodID())

    def left_entry180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin180(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin180(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim180(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim180(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale180(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim180(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim180(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax180(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax180(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax180(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_190 ──────────────────────────────────────────────────────────────────

    def focusout_entry190(self, event=None):
        self.entry(self.getMethodID())

    def down_entry190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry190(self, event=None):
        self.entry(self.getMethodID())

    def left_entry190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin190(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin190(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim190(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim190(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale190(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim190(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim190(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax190(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax190(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax190(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_200 ──────────────────────────────────────────────────────────────────

    def focusout_entry200(self, event=None):
        self.entry(self.getMethodID())

    def down_entry200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry200(self, event=None):
        self.entry(self.getMethodID())

    def left_entry200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin200(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin200(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim200(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim200(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale200(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim200(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim200(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax200(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax200(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax200(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_210 ──────────────────────────────────────────────────────────────────

    def focusout_entry210(self, event=None):
        self.entry(self.getMethodID())

    def down_entry210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry210(self, event=None):
        self.entry(self.getMethodID())

    def left_entry210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin210(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin210(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim210(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim210(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale210(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim210(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim210(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax210(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax210(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax210(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_220 ──────────────────────────────────────────────────────────────────

    def focusout_entry220(self, event=None):
        self.entry(self.getMethodID())

    def down_entry220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry220(self, event=None):
        self.entry(self.getMethodID())

    def left_entry220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin220(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin220(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim220(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim220(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale220(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim220(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim220(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax220(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax220(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax220(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_230 ──────────────────────────────────────────────────────────────────

    def focusout_entry230(self, event=None):
        self.entry(self.getMethodID())

    def down_entry230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry230(self, event=None):
        self.entry(self.getMethodID())

    def left_entry230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin230(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin230(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim230(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim230(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale230(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim230(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim230(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax230(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax230(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax230(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_240 ──────────────────────────────────────────────────────────────────

    def focusout_entry240(self, event=None):
        self.entry(self.getMethodID())

    def down_entry240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry240(self, event=None):
        self.entry(self.getMethodID())

    def left_entry240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin240(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin240(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim240(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim240(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale240(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim240(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim240(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax240(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax240(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax240(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_250 ──────────────────────────────────────────────────────────────────

    def focusout_entry250(self, event=None):
        self.entry(self.getMethodID())

    def down_entry250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry250(self, event=None):
        self.entry(self.getMethodID())

    def left_entry250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin250(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin250(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim250(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim250(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale250(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim250(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim250(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax250(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax250(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax250(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_260 ──────────────────────────────────────────────────────────────────

    def focusout_entry260(self, event=None):
        self.entry(self.getMethodID())

    def down_entry260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry260(self, event=None):
        self.entry(self.getMethodID())

    def left_entry260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin260(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin260(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim260(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim260(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale260(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim260(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim260(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax260(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax260(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax260(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_270 ──────────────────────────────────────────────────────────────────

    def focusout_entry270(self, event=None):
        self.entry(self.getMethodID())

    def down_entry270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry270(self, event=None):
        self.entry(self.getMethodID())

    def left_entry270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin270(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin270(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim270(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim270(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale270(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim270(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim270(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax270(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax270(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax270(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_280 ──────────────────────────────────────────────────────────────────

    def focusout_entry280(self, event=None):
        self.entry(self.getMethodID())

    def down_entry280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry280(self, event=None):
        self.entry(self.getMethodID())

    def left_entry280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin280(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin280(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim280(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim280(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale280(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim280(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim280(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax280(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax280(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax280(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_290 ──────────────────────────────────────────────────────────────────

    def focusout_entry290(self, event=None):
        self.entry(self.getMethodID())

    def down_entry290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry290(self, event=None):
        self.entry(self.getMethodID())

    def left_entry290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin290(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin290(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim290(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim290(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale290(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim290(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim290(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax290(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax290(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax290(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_300 ──────────────────────────────────────────────────────────────────

    def focusout_entry300(self, event=None):
        self.entry(self.getMethodID())

    def down_entry300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry300(self, event=None):
        self.entry(self.getMethodID())

    def left_entry300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin300(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin300(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim300(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim300(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale300(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim300(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim300(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax300(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax300(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax300(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_310 ──────────────────────────────────────────────────────────────────

    def focusout_entry310(self, event=None):
        self.entry(self.getMethodID())

    def down_entry310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry310(self, event=None):
        self.entry(self.getMethodID())

    def left_entry310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin310(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin310(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim310(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim310(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale310(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim310(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim310(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax310(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax310(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax310(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_320 ──────────────────────────────────────────────────────────────────

    def focusout_entry320(self, event=None):
        self.entry(self.getMethodID())

    def down_entry320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry320(self, event=None):
        self.entry(self.getMethodID())

    def left_entry320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin320(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin320(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim320(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim320(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale320(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim320(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim320(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax320(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax320(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax320(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_330 ──────────────────────────────────────────────────────────────────

    def focusout_entry330(self, event=None):
        self.entry(self.getMethodID())

    def down_entry330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry330(self, event=None):
        self.entry(self.getMethodID())

    def left_entry330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin330(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin330(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim330(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim330(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale330(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim330(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim330(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax330(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax330(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax330(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_340 ──────────────────────────────────────────────────────────────────

    def focusout_entry340(self, event=None):
        self.entry(self.getMethodID())

    def down_entry340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry340(self, event=None):
        self.entry(self.getMethodID())

    def left_entry340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin340(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin340(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim340(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim340(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale340(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim340(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim340(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax340(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax340(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax340(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_350 ──────────────────────────────────────────────────────────────────

    def focusout_entry350(self, event=None):
        self.entry(self.getMethodID())

    def down_entry350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry350(self, event=None):
        self.entry(self.getMethodID())

    def left_entry350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin350(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin350(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim350(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim350(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale350(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim350(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim350(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax350(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax350(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax350(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_360 ──────────────────────────────────────────────────────────────────

    def focusout_entry360(self, event=None):
        self.entry(self.getMethodID())

    def down_entry360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry360(self, event=None):
        self.entry(self.getMethodID())

    def left_entry360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin360(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin360(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim360(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim360(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale360(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim360(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim360(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax360(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax360(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax360(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_370 ──────────────────────────────────────────────────────────────────

    def focusout_entry370(self, event=None):
        self.entry(self.getMethodID())

    def down_entry370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry370(self, event=None):
        self.entry(self.getMethodID())

    def left_entry370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin370(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin370(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim370(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim370(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale370(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim370(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim370(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax370(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax370(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax370(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_380 ──────────────────────────────────────────────────────────────────

    def focusout_entry380(self, event=None):
        self.entry(self.getMethodID())

    def down_entry380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry380(self, event=None):
        self.entry(self.getMethodID())

    def left_entry380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin380(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin380(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim380(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim380(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale380(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim380(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim380(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax380(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax380(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax380(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_390 ──────────────────────────────────────────────────────────────────

    def focusout_entry390(self, event=None):
        self.entry(self.getMethodID())

    def down_entry390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry390(self, event=None):
        self.entry(self.getMethodID())

    def left_entry390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin390(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin390(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim390(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim390(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale390(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim390(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim390(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax390(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax390(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax390(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_400 ──────────────────────────────────────────────────────────────────

    def focusout_entry400(self, event=None):
        self.entry(self.getMethodID())

    def down_entry400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry400(self, event=None):
        self.entry(self.getMethodID())

    def left_entry400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin400(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin400(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim400(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim400(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale400(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim400(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim400(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax400(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax400(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax400(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_410 ──────────────────────────────────────────────────────────────────

    def focusout_entry410(self, event=None):
        self.entry(self.getMethodID())

    def down_entry410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry410(self, event=None):
        self.entry(self.getMethodID())

    def left_entry410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin410(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin410(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim410(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim410(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale410(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim410(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim410(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax410(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax410(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax410(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_420 ──────────────────────────────────────────────────────────────────

    def focusout_entry420(self, event=None):
        self.entry(self.getMethodID())

    def down_entry420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry420(self, event=None):
        self.entry(self.getMethodID())

    def left_entry420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin420(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin420(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim420(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim420(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale420(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim420(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim420(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax420(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax420(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax420(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_430 ──────────────────────────────────────────────────────────────────

    def focusout_entry430(self, event=None):
        self.entry(self.getMethodID())

    def down_entry430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry430(self, event=None):
        self.entry(self.getMethodID())

    def left_entry430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin430(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin430(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim430(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim430(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale430(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim430(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim430(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax430(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax430(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax430(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_440 ──────────────────────────────────────────────────────────────────

    def focusout_entry440(self, event=None):
        self.entry(self.getMethodID())

    def down_entry440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry440(self, event=None):
        self.entry(self.getMethodID())

    def left_entry440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin440(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin440(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim440(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim440(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale440(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim440(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim440(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax440(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax440(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax440(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_450 ──────────────────────────────────────────────────────────────────

    def focusout_entry450(self, event=None):
        self.entry(self.getMethodID())

    def down_entry450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry450(self, event=None):
        self.entry(self.getMethodID())

    def left_entry450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin450(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin450(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim450(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim450(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale450(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim450(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim450(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax450(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax450(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax450(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_460 ──────────────────────────────────────────────────────────────────

    def focusout_entry460(self, event=None):
        self.entry(self.getMethodID())

    def down_entry460(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry460(self, event=None):
        self.entry(self.getMethodID())

    def left_entry460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry460(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin460(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin460(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin460(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin460(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim460(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim460(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim460(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim460(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale460(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim460(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim460(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim460(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim460(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax460(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax460(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax460(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax460(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax460(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax460(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_470 ──────────────────────────────────────────────────────────────────

    def focusout_entry470(self, event=None):
        self.entry(self.getMethodID())

    def down_entry470(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry470(self, event=None):
        self.entry(self.getMethodID())

    def left_entry470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry470(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin470(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin470(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin470(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin470(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim470(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim470(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim470(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim470(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale470(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim470(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim470(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim470(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim470(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax470(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax470(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax470(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax470(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax470(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax470(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_480 ──────────────────────────────────────────────────────────────────

    def focusout_entry480(self, event=None):
        self.entry(self.getMethodID())

    def down_entry480(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry480(self, event=None):
        self.entry(self.getMethodID())

    def left_entry480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry480(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin480(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin480(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin480(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin480(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim480(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim480(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim480(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim480(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale480(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim480(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim480(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim480(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim480(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax480(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax480(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax480(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax480(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax480(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax480(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_490 ──────────────────────────────────────────────────────────────────

    def focusout_entry490(self, event=None):
        self.entry(self.getMethodID())

    def down_entry490(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry490(self, event=None):
        self.entry(self.getMethodID())

    def left_entry490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry490(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin490(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin490(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin490(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin490(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim490(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim490(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim490(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim490(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale490(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim490(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim490(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim490(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim490(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax490(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax490(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax490(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax490(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax490(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax490(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_500 ──────────────────────────────────────────────────────────────────

    def focusout_entry500(self, event=None):
        self.entry(self.getMethodID())

    def down_entry500(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry500(self, event=None):
        self.entry(self.getMethodID())

    def left_entry500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry500(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin500(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin500(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin500(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin500(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim500(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim500(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim500(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim500(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale500(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim500(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim500(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim500(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim500(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax500(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax500(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax500(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax500(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax500(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax500(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_510 ──────────────────────────────────────────────────────────────────

    def focusout_entry510(self, event=None):
        self.entry(self.getMethodID())

    def down_entry510(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry510(self, event=None):
        self.entry(self.getMethodID())

    def left_entry510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry510(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin510(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin510(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin510(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin510(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim510(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim510(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim510(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim510(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale510(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim510(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim510(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim510(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim510(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax510(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax510(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax510(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax510(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax510(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax510(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_520 ──────────────────────────────────────────────────────────────────

    def focusout_entry520(self, event=None):
        self.entry(self.getMethodID())

    def down_entry520(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry520(self, event=None):
        self.entry(self.getMethodID())

    def left_entry520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry520(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin520(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin520(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin520(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin520(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim520(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim520(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim520(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim520(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale520(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim520(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim520(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim520(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim520(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax520(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax520(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax520(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax520(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax520(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax520(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_530 ──────────────────────────────────────────────────────────────────

    def focusout_entry530(self, event=None):
        self.entry(self.getMethodID())

    def down_entry530(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry530(self, event=None):
        self.entry(self.getMethodID())

    def left_entry530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry530(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin530(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin530(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin530(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin530(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim530(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim530(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim530(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim530(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale530(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim530(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim530(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim530(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim530(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax530(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax530(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax530(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax530(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax530(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax530(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_540 ──────────────────────────────────────────────────────────────────

    def focusout_entry540(self, event=None):
        self.entry(self.getMethodID())

    def down_entry540(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry540(self, event=None):
        self.entry(self.getMethodID())

    def left_entry540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry540(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin540(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin540(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin540(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin540(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim540(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim540(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim540(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim540(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale540(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim540(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim540(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim540(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim540(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax540(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax540(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax540(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax540(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax540(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax540(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_550 ──────────────────────────────────────────────────────────────────

    def focusout_entry550(self, event=None):
        self.entry(self.getMethodID())

    def down_entry550(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry550(self, event=None):
        self.entry(self.getMethodID())

    def left_entry550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry550(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin550(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin550(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin550(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin550(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim550(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim550(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim550(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim550(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale550(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim550(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim550(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim550(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim550(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax550(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax550(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax550(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax550(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax550(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax550(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_560 ──────────────────────────────────────────────────────────────────

    def focusout_entry560(self, event=None):
        self.entry(self.getMethodID())

    def down_entry560(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry560(self, event=None):
        self.entry(self.getMethodID())

    def left_entry560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry560(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin560(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin560(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin560(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin560(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim560(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim560(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim560(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim560(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale560(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim560(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim560(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim560(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim560(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax560(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax560(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax560(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax560(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax560(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax560(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_570 ──────────────────────────────────────────────────────────────────

    def focusout_entry570(self, event=None):
        self.entry(self.getMethodID())

    def down_entry570(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry570(self, event=None):
        self.entry(self.getMethodID())

    def left_entry570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry570(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin570(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin570(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin570(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin570(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim570(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim570(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim570(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim570(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale570(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim570(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim570(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim570(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim570(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax570(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax570(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax570(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax570(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax570(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax570(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_580 ──────────────────────────────────────────────────────────────────

    def focusout_entry580(self, event=None):
        self.entry(self.getMethodID())

    def down_entry580(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry580(self, event=None):
        self.entry(self.getMethodID())

    def left_entry580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry580(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin580(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin580(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin580(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin580(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim580(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim580(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim580(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim580(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale580(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim580(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim580(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim580(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim580(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax580(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax580(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax580(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax580(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax580(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax580(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_590 ──────────────────────────────────────────────────────────────────

    def focusout_entry590(self, event=None):
        self.entry(self.getMethodID())

    def down_entry590(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry590(self, event=None):
        self.entry(self.getMethodID())

    def left_entry590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry590(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin590(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin590(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin590(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin590(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim590(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim590(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim590(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim590(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale590(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim590(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim590(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim590(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim590(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax590(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax590(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax590(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax590(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax590(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax590(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_600 ──────────────────────────────────────────────────────────────────

    def focusout_entry600(self, event=None):
        self.entry(self.getMethodID())

    def down_entry600(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry600(self, event=None):
        self.entry(self.getMethodID())

    def left_entry600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry600(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin600(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin600(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin600(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin600(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim600(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim600(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim600(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim600(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale600(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim600(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim600(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim600(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim600(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax600(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax600(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax600(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax600(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax600(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax600(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_610 ──────────────────────────────────────────────────────────────────

    def focusout_entry610(self, event=None):
        self.entry(self.getMethodID())

    def down_entry610(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry610(self, event=None):
        self.entry(self.getMethodID())

    def left_entry610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry610(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin610(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin610(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin610(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin610(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim610(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim610(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim610(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim610(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale610(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim610(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim610(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim610(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim610(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax610(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax610(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax610(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax610(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax610(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax610(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_620 ──────────────────────────────────────────────────────────────────

    def focusout_entry620(self, event=None):
        self.entry(self.getMethodID())

    def down_entry620(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry620(self, event=None):
        self.entry(self.getMethodID())

    def left_entry620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry620(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin620(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin620(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin620(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin620(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim620(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim620(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim620(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim620(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale620(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim620(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim620(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim620(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim620(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax620(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax620(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax620(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax620(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax620(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax620(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_630 ──────────────────────────────────────────────────────────────────

    def focusout_entry630(self, event=None):
        self.entry(self.getMethodID())

    def down_entry630(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry630(self, event=None):
        self.entry(self.getMethodID())

    def left_entry630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry630(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin630(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin630(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin630(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin630(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim630(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim630(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim630(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim630(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale630(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim630(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim630(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim630(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim630(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax630(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax630(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax630(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax630(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax630(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax630(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_640 ──────────────────────────────────────────────────────────────────

    def focusout_entry640(self, event=None):
        self.entry(self.getMethodID())

    def down_entry640(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry640(self, event=None):
        self.entry(self.getMethodID())

    def left_entry640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry640(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin640(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin640(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin640(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin640(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim640(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim640(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim640(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim640(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale640(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim640(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim640(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim640(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim640(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax640(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax640(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax640(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax640(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax640(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax640(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_650 ──────────────────────────────────────────────────────────────────

    def focusout_entry650(self, event=None):
        self.entry(self.getMethodID())

    def down_entry650(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry650(self, event=None):
        self.entry(self.getMethodID())

    def left_entry650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry650(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin650(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin650(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin650(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin650(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim650(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim650(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim650(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim650(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale650(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim650(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim650(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim650(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim650(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax650(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax650(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax650(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax650(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax650(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax650(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_660 ──────────────────────────────────────────────────────────────────

    def focusout_entry660(self, event=None):
        self.entry(self.getMethodID())

    def down_entry660(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry660(self, event=None):
        self.entry(self.getMethodID())

    def left_entry660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry660(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin660(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin660(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin660(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin660(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim660(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim660(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim660(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim660(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale660(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim660(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim660(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim660(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim660(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax660(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax660(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax660(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax660(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax660(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax660(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_670 ──────────────────────────────────────────────────────────────────

    def focusout_entry670(self, event=None):
        self.entry(self.getMethodID())

    def down_entry670(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry670(self, event=None):
        self.entry(self.getMethodID())

    def left_entry670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry670(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin670(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin670(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin670(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin670(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim670(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim670(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim670(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim670(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale670(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim670(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim670(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim670(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim670(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax670(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax670(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax670(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax670(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax670(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax670(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_680 ──────────────────────────────────────────────────────────────────

    def focusout_entry680(self, event=None):
        self.entry(self.getMethodID())

    def down_entry680(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry680(self, event=None):
        self.entry(self.getMethodID())

    def left_entry680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry680(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin680(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin680(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin680(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin680(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim680(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim680(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim680(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim680(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale680(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim680(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim680(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim680(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim680(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax680(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax680(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax680(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax680(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax680(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax680(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_690 ──────────────────────────────────────────────────────────────────

    def focusout_entry690(self, event=None):
        self.entry(self.getMethodID())

    def down_entry690(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry690(self, event=None):
        self.entry(self.getMethodID())

    def left_entry690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry690(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin690(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin690(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin690(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin690(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim690(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim690(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim690(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim690(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale690(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim690(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim690(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim690(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim690(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax690(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax690(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax690(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax690(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax690(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax690(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_700 ──────────────────────────────────────────────────────────────────

    def focusout_entry700(self, event=None):
        self.entry(self.getMethodID())

    def down_entry700(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry700(self, event=None):
        self.entry(self.getMethodID())

    def left_entry700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry700(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin700(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin700(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin700(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin700(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim700(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim700(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim700(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim700(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale700(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim700(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim700(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim700(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim700(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax700(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax700(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax700(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax700(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax700(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax700(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_710 ──────────────────────────────────────────────────────────────────

    def focusout_entry710(self, event=None):
        self.entry(self.getMethodID())

    def down_entry710(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry710(self, event=None):
        self.entry(self.getMethodID())

    def left_entry710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry710(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin710(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin710(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin710(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin710(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim710(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim710(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim710(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim710(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale710(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim710(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim710(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim710(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim710(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax710(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax710(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax710(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax710(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax710(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax710(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_720 ──────────────────────────────────────────────────────────────────

    def focusout_entry720(self, event=None):
        self.entry(self.getMethodID())

    def down_entry720(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry720(self, event=None):
        self.entry(self.getMethodID())

    def left_entry720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry720(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin720(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin720(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin720(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin720(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim720(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim720(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim720(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim720(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale720(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim720(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim720(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim720(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim720(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax720(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax720(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax720(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax720(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax720(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax720(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_730 ──────────────────────────────────────────────────────────────────

    def focusout_entry730(self, event=None):
        self.entry(self.getMethodID())

    def down_entry730(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry730(self, event=None):
        self.entry(self.getMethodID())

    def left_entry730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry730(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin730(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin730(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin730(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin730(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim730(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim730(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim730(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim730(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale730(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim730(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim730(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim730(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim730(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax730(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax730(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax730(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax730(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax730(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax730(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_740 ──────────────────────────────────────────────────────────────────

    def focusout_entry740(self, event=None):
        self.entry(self.getMethodID())

    def down_entry740(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry740(self, event=None):
        self.entry(self.getMethodID())

    def left_entry740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry740(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin740(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin740(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin740(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin740(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim740(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim740(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim740(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim740(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale740(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim740(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim740(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim740(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim740(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax740(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax740(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax740(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax740(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax740(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax740(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_750 ──────────────────────────────────────────────────────────────────

    def focusout_entry750(self, event=None):
        self.entry(self.getMethodID())

    def down_entry750(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry750(self, event=None):
        self.entry(self.getMethodID())

    def left_entry750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry750(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin750(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin750(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin750(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin750(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim750(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim750(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim750(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim750(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale750(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim750(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim750(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim750(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim750(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax750(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax750(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax750(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax750(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax750(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax750(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_760 ──────────────────────────────────────────────────────────────────

    def focusout_entry760(self, event=None):
        self.entry(self.getMethodID())

    def down_entry760(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry760(self, event=None):
        self.entry(self.getMethodID())

    def left_entry760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry760(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin760(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin760(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin760(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin760(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim760(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim760(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim760(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim760(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale760(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim760(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim760(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim760(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim760(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax760(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax760(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax760(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax760(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax760(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax760(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_770 ──────────────────────────────────────────────────────────────────

    def focusout_entry770(self, event=None):
        self.entry(self.getMethodID())

    def down_entry770(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry770(self, event=None):
        self.entry(self.getMethodID())

    def left_entry770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry770(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin770(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin770(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin770(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin770(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim770(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim770(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim770(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim770(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale770(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim770(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim770(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim770(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim770(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax770(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax770(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax770(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax770(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax770(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax770(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_780 ──────────────────────────────────────────────────────────────────

    def focusout_entry780(self, event=None):
        self.entry(self.getMethodID())

    def down_entry780(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry780(self, event=None):
        self.entry(self.getMethodID())

    def left_entry780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry780(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin780(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin780(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin780(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin780(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim780(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim780(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim780(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim780(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale780(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim780(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim780(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim780(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim780(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax780(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax780(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax780(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax780(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax780(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax780(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_790 ──────────────────────────────────────────────────────────────────

    def focusout_entry790(self, event=None):
        self.entry(self.getMethodID())

    def down_entry790(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry790(self, event=None):
        self.entry(self.getMethodID())

    def left_entry790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry790(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin790(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin790(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin790(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin790(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim790(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim790(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim790(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim790(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale790(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim790(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim790(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim790(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim790(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax790(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax790(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax790(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax790(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax790(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax790(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_800 ──────────────────────────────────────────────────────────────────

    def focusout_entry800(self, event=None):
        self.entry(self.getMethodID())

    def down_entry800(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry800(self, event=None):
        self.entry(self.getMethodID())

    def left_entry800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry800(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin800(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin800(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin800(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin800(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim800(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim800(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim800(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim800(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale800(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim800(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim800(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim800(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim800(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax800(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax800(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax800(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax800(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax800(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax800(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_810 ──────────────────────────────────────────────────────────────────

    def focusout_entry810(self, event=None):
        self.entry(self.getMethodID())

    def down_entry810(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry810(self, event=None):
        self.entry(self.getMethodID())

    def left_entry810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry810(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin810(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin810(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin810(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin810(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim810(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim810(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim810(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim810(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale810(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim810(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim810(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim810(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim810(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax810(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax810(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax810(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax810(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax810(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax810(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_820 ──────────────────────────────────────────────────────────────────

    def focusout_entry820(self, event=None):
        self.entry(self.getMethodID())

    def down_entry820(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry820(self, event=None):
        self.entry(self.getMethodID())

    def left_entry820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry820(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin820(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin820(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin820(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin820(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim820(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim820(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim820(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim820(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale820(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim820(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim820(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim820(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim820(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax820(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax820(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax820(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax820(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax820(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax820(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_830 ──────────────────────────────────────────────────────────────────

    def focusout_entry830(self, event=None):
        self.entry(self.getMethodID())

    def down_entry830(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry830(self, event=None):
        self.entry(self.getMethodID())

    def left_entry830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry830(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin830(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin830(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin830(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin830(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim830(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim830(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim830(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim830(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale830(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim830(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim830(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim830(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim830(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax830(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax830(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax830(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax830(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax830(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax830(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_840 ──────────────────────────────────────────────────────────────────

    def focusout_entry840(self, event=None):
        self.entry(self.getMethodID())

    def down_entry840(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry840(self, event=None):
        self.entry(self.getMethodID())

    def left_entry840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry840(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin840(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin840(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin840(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin840(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim840(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim840(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim840(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim840(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale840(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim840(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim840(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim840(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim840(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax840(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax840(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax840(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax840(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax840(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax840(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_850 ──────────────────────────────────────────────────────────────────

    def focusout_entry850(self, event=None):
        self.entry(self.getMethodID())

    def down_entry850(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry850(self, event=None):
        self.entry(self.getMethodID())

    def left_entry850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry850(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin850(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin850(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin850(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin850(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim850(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim850(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim850(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim850(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale850(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim850(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim850(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim850(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim850(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax850(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax850(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax850(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax850(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax850(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax850(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_860 ──────────────────────────────────────────────────────────────────

    def focusout_entry860(self, event=None):
        self.entry(self.getMethodID())

    def down_entry860(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry860(self, event=None):
        self.entry(self.getMethodID())

    def left_entry860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry860(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin860(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin860(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin860(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin860(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim860(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim860(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim860(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim860(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale860(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim860(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim860(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim860(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim860(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax860(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax860(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax860(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax860(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax860(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax860(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_870 ──────────────────────────────────────────────────────────────────

    def focusout_entry870(self, event=None):
        self.entry(self.getMethodID())

    def down_entry870(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry870(self, event=None):
        self.entry(self.getMethodID())

    def left_entry870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry870(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin870(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin870(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin870(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin870(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim870(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim870(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim870(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim870(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale870(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim870(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim870(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim870(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim870(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax870(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax870(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax870(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax870(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax870(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax870(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_880 ──────────────────────────────────────────────────────────────────

    def focusout_entry880(self, event=None):
        self.entry(self.getMethodID())

    def down_entry880(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry880(self, event=None):
        self.entry(self.getMethodID())

    def left_entry880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry880(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin880(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin880(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin880(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin880(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim880(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim880(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim880(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim880(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale880(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim880(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim880(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim880(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim880(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax880(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax880(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax880(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax880(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax880(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax880(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_890 ──────────────────────────────────────────────────────────────────

    def focusout_entry890(self, event=None):
        self.entry(self.getMethodID())

    def down_entry890(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry890(self, event=None):
        self.entry(self.getMethodID())

    def left_entry890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry890(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin890(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin890(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin890(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin890(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim890(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim890(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim890(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim890(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale890(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim890(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim890(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim890(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim890(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax890(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax890(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax890(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax890(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax890(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax890(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_900 ──────────────────────────────────────────────────────────────────

    def focusout_entry900(self, event=None):
        self.entry(self.getMethodID())

    def down_entry900(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry900(self, event=None):
        self.entry(self.getMethodID())

    def left_entry900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry900(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin900(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin900(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin900(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin900(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim900(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim900(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim900(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim900(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale900(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim900(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim900(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim900(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim900(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax900(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax900(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax900(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax900(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax900(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax900(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_910 ──────────────────────────────────────────────────────────────────

    def focusout_entry910(self, event=None):
        self.entry(self.getMethodID())

    def down_entry910(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry910(self, event=None):
        self.entry(self.getMethodID())

    def left_entry910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry910(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin910(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin910(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin910(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin910(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim910(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim910(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim910(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim910(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale910(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim910(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim910(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim910(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim910(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax910(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax910(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax910(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax910(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax910(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax910(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_920 ──────────────────────────────────────────────────────────────────

    def focusout_entry920(self, event=None):
        self.entry(self.getMethodID())

    def down_entry920(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry920(self, event=None):
        self.entry(self.getMethodID())

    def left_entry920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry920(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin920(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin920(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin920(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin920(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim920(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim920(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim920(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim920(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale920(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim920(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim920(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim920(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim920(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax920(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax920(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax920(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax920(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax920(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax920(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_930 ──────────────────────────────────────────────────────────────────

    def focusout_entry930(self, event=None):
        self.entry(self.getMethodID())

    def down_entry930(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry930(self, event=None):
        self.entry(self.getMethodID())

    def left_entry930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry930(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin930(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin930(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin930(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin930(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim930(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim930(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim930(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim930(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale930(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim930(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim930(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim930(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim930(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax930(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax930(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax930(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax930(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax930(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax930(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_940 ──────────────────────────────────────────────────────────────────

    def focusout_entry940(self, event=None):
        self.entry(self.getMethodID())

    def down_entry940(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry940(self, event=None):
        self.entry(self.getMethodID())

    def left_entry940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry940(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin940(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin940(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin940(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin940(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim940(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim940(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim940(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim940(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale940(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim940(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim940(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim940(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim940(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax940(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax940(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax940(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax940(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax940(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax940(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_950 ──────────────────────────────────────────────────────────────────

    def focusout_entry950(self, event=None):
        self.entry(self.getMethodID())

    def down_entry950(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry950(self, event=None):
        self.entry(self.getMethodID())

    def left_entry950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry950(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin950(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin950(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin950(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin950(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim950(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim950(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim950(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim950(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale950(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim950(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim950(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim950(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim950(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax950(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax950(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax950(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax950(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax950(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax950(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_960 ──────────────────────────────────────────────────────────────────

    def focusout_entry960(self, event=None):
        self.entry(self.getMethodID())

    def down_entry960(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry960(self, event=None):
        self.entry(self.getMethodID())

    def left_entry960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry960(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin960(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin960(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin960(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin960(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim960(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim960(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim960(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim960(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale960(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim960(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim960(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim960(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim960(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax960(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax960(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax960(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax960(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax960(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax960(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_970 ──────────────────────────────────────────────────────────────────

    def focusout_entry970(self, event=None):
        self.entry(self.getMethodID())

    def down_entry970(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry970(self, event=None):
        self.entry(self.getMethodID())

    def left_entry970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry970(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin970(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin970(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin970(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin970(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim970(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim970(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim970(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim970(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale970(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim970(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim970(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim970(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim970(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax970(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax970(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax970(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax970(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax970(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax970(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_980 ──────────────────────────────────────────────────────────────────

    def focusout_entry980(self, event=None):
        self.entry(self.getMethodID())

    def down_entry980(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry980(self, event=None):
        self.entry(self.getMethodID())

    def left_entry980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry980(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin980(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin980(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin980(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin980(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim980(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim980(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim980(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim980(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale980(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim980(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim980(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim980(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim980(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax980(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax980(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax980(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax980(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax980(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax980(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_990 ──────────────────────────────────────────────────────────────────

    def focusout_entry990(self, event=None):
        self.entry(self.getMethodID())

    def down_entry990(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry990(self, event=None):
        self.entry(self.getMethodID())

    def left_entry990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry990(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin990(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin990(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin990(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin990(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim990(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim990(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim990(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim990(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale990(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim990(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim990(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim990(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim990(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax990(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax990(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax990(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax990(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax990(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax990(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1000 ──────────────────────────────────────────────────────────────────

    def focusout_entry1000(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1000(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1000(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1000(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1000(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1000(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1000(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1000(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1000(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1000(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1000(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1000(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1000(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1000(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1000(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1000(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1000(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1000(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1000(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1000(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1000(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1000(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1000(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1010 ──────────────────────────────────────────────────────────────────

    def focusout_entry1010(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1010(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1010(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1010(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1010(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1010(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1010(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1010(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1010(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1010(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1010(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1010(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1010(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1010(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1010(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1010(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1010(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1010(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1010(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1010(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1010(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1010(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1010(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1020 ──────────────────────────────────────────────────────────────────

    def focusout_entry1020(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1020(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1020(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1020(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1020(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1020(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1020(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1020(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1020(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1020(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1020(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1020(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1020(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1020(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1020(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1020(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1020(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1020(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1020(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1020(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1020(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1020(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1020(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1030 ──────────────────────────────────────────────────────────────────

    def focusout_entry1030(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1030(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1030(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1030(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1030(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1030(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1030(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1030(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1030(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1030(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1030(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1030(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1030(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1030(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1030(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1030(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1030(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1030(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1030(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1030(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1030(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1030(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1030(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1040 ──────────────────────────────────────────────────────────────────

    def focusout_entry1040(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1040(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1040(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1040(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1040(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1040(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1040(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1040(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1040(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1040(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1040(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1040(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1040(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1040(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1040(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1040(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1040(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1040(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1040(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1040(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1040(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1040(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1040(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1050 ──────────────────────────────────────────────────────────────────

    def focusout_entry1050(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1050(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1050(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1050(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1050(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1050(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1050(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1050(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1050(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1050(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1050(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1050(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1050(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1050(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1050(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1050(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1050(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1050(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1050(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1050(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1050(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1050(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1050(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1060 ──────────────────────────────────────────────────────────────────

    def focusout_entry1060(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1060(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1060(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1060(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1060(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1060(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1060(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1060(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1060(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1060(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1060(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1060(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1060(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1060(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1060(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1060(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1060(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1060(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1060(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1060(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1060(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1060(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1060(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1070 ──────────────────────────────────────────────────────────────────

    def focusout_entry1070(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1070(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1070(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1070(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1070(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1070(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1070(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1070(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1070(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1070(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1070(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1070(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1070(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1070(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1070(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1070(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1070(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1070(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1070(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1070(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1070(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1070(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1070(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1080 ──────────────────────────────────────────────────────────────────

    def focusout_entry1080(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1080(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1080(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1080(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1080(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1080(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1080(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1080(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1080(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1080(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1080(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1080(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1080(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1080(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1080(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1080(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1080(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1080(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1080(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1080(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1080(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1080(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1080(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1090 ──────────────────────────────────────────────────────────────────

    def focusout_entry1090(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1090(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1090(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1090(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1090(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1090(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1090(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1090(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1090(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1090(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1090(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1090(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1090(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1090(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1090(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1090(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1090(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1090(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1090(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1090(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1090(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1090(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1090(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1100 ──────────────────────────────────────────────────────────────────

    def focusout_entry1100(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1100(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1100(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1100(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1100(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1100(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1100(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1100(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1100(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1100(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1100(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1100(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1100(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1100(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1100(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1100(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1110 ──────────────────────────────────────────────────────────────────

    def focusout_entry1110(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1110(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1110(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1110(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1110(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1110(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1110(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1110(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1110(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1110(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1110(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1110(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1110(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1110(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1110(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1110(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1120 ──────────────────────────────────────────────────────────────────

    def focusout_entry1120(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1120(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1120(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1120(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1120(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1120(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1120(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1120(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1120(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1120(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1120(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1120(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1120(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1120(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1120(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1120(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1130 ──────────────────────────────────────────────────────────────────

    def focusout_entry1130(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1130(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1130(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1130(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1130(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1130(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1130(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1130(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1130(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1130(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1130(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1130(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1130(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1130(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1130(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1130(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1140 ──────────────────────────────────────────────────────────────────

    def focusout_entry1140(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1140(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1140(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1140(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1140(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1140(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1140(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1140(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1140(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1140(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1140(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1140(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1140(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1140(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1140(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1140(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1150 ──────────────────────────────────────────────────────────────────

    def focusout_entry1150(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1150(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1150(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1150(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1150(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1150(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1150(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1150(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1150(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1150(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1150(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1150(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1150(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1150(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1150(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1150(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1160 ──────────────────────────────────────────────────────────────────

    def focusout_entry1160(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1160(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1160(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1160(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1160(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1160(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1160(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1160(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1160(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1160(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1160(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1160(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1160(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1160(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1160(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1160(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1170 ──────────────────────────────────────────────────────────────────

    def focusout_entry1170(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1170(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1170(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1170(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1170(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1170(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1170(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1170(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1170(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1170(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1170(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1170(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1170(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1170(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1170(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1170(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1180 ──────────────────────────────────────────────────────────────────

    def focusout_entry1180(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1180(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1180(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1180(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1180(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1180(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1180(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1180(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1180(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1180(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1180(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1180(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1180(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1180(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1180(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1180(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1190 ──────────────────────────────────────────────────────────────────

    def focusout_entry1190(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1190(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1190(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1190(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1190(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1190(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1190(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1190(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1190(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1190(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1190(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1190(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1190(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1190(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1190(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1190(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1200 ──────────────────────────────────────────────────────────────────

    def focusout_entry1200(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1200(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1200(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1200(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1200(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1200(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1200(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1200(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1200(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1200(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1200(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1200(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1200(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1200(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1200(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1200(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1210 ──────────────────────────────────────────────────────────────────

    def focusout_entry1210(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1210(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1210(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1210(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1210(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1210(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1210(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1210(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1210(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1210(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1210(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1210(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1210(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1210(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1210(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1210(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1220 ──────────────────────────────────────────────────────────────────

    def focusout_entry1220(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1220(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1220(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1220(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1220(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1220(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1220(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1220(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1220(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1220(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1220(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1220(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1220(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1220(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1220(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1220(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1230 ──────────────────────────────────────────────────────────────────

    def focusout_entry1230(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1230(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1230(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1230(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1230(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1230(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1230(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1230(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1230(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1230(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1230(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1230(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1230(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1230(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1230(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1230(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1240 ──────────────────────────────────────────────────────────────────

    def focusout_entry1240(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1240(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1240(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1240(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1240(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1240(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1240(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1240(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1240(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1240(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1240(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1240(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1240(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1240(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1240(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1240(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1250 ──────────────────────────────────────────────────────────────────

    def focusout_entry1250(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1250(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1250(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1250(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1250(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1250(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1250(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1250(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1250(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1250(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1250(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1250(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1250(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1250(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1250(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1250(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1260 ──────────────────────────────────────────────────────────────────

    def focusout_entry1260(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1260(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1260(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1260(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1260(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1260(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1260(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1260(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1260(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1260(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1260(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1260(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1260(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1260(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1260(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1260(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1270 ──────────────────────────────────────────────────────────────────

    def focusout_entry1270(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1270(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1270(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1270(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1270(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1270(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1270(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1270(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1270(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1270(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1270(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1270(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1270(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1270(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1270(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1270(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1280 ──────────────────────────────────────────────────────────────────

    def focusout_entry1280(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1280(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1280(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1280(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1280(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1280(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1280(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1280(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1280(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1280(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1280(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1280(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1280(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1280(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1280(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1280(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1290 ──────────────────────────────────────────────────────────────────

    def focusout_entry1290(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1290(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1290(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1290(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1290(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1290(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1290(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1290(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1290(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1290(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1290(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1290(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1290(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1290(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1290(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1290(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1300 ──────────────────────────────────────────────────────────────────

    def focusout_entry1300(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1300(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1300(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1300(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1300(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1300(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1300(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1300(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1300(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1300(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1300(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1300(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1300(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1300(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1300(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1300(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1310 ──────────────────────────────────────────────────────────────────

    def focusout_entry1310(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1310(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1310(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1310(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1310(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1310(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1310(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1310(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1310(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1310(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1310(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1310(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1310(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1310(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1310(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1310(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1320 ──────────────────────────────────────────────────────────────────

    def focusout_entry1320(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1320(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1320(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1320(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1320(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1320(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1320(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1320(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1320(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1320(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1320(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1320(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1320(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1320(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1320(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1320(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1330 ──────────────────────────────────────────────────────────────────

    def focusout_entry1330(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1330(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1330(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1330(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1330(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1330(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1330(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1330(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1330(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1330(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1330(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1330(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1330(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1330(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1330(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1330(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1340 ──────────────────────────────────────────────────────────────────

    def focusout_entry1340(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1340(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1340(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1340(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1340(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1340(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1340(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1340(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1340(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1340(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1340(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1340(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1340(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1340(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1340(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1340(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1350 ──────────────────────────────────────────────────────────────────

    def focusout_entry1350(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1350(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1350(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1350(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1350(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1350(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1350(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1350(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1350(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1350(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1350(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1350(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1350(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1350(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1350(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1350(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1360 ──────────────────────────────────────────────────────────────────

    def focusout_entry1360(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1360(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1360(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1360(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1360(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1360(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1360(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1360(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1360(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1360(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1360(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1360(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1360(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1360(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1360(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1360(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1370 ──────────────────────────────────────────────────────────────────

    def focusout_entry1370(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1370(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1370(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1370(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1370(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1370(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1370(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1370(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1370(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1370(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1370(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1370(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1370(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1370(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1370(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1370(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1380 ──────────────────────────────────────────────────────────────────

    def focusout_entry1380(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1380(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1380(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1380(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1380(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1380(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1380(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1380(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1380(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1380(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1380(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1380(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1380(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1380(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1380(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1380(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1390 ──────────────────────────────────────────────────────────────────

    def focusout_entry1390(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1390(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1390(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1390(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1390(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1390(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1390(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1390(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1390(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1390(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1390(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1390(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1390(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1390(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1390(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1390(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1400 ──────────────────────────────────────────────────────────────────

    def focusout_entry1400(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1400(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1400(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1400(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1400(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1400(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1400(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1400(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1400(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1400(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1400(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1400(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1400(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1400(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1400(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1400(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1410 ──────────────────────────────────────────────────────────────────

    def focusout_entry1410(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1410(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1410(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1410(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1410(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1410(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1410(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1410(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1410(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1410(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1410(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1410(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1410(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1410(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1410(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1410(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1420 ──────────────────────────────────────────────────────────────────

    def focusout_entry1420(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1420(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1420(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1420(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1420(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1420(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1420(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1420(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1420(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1420(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1420(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1420(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1420(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1420(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1420(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1420(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1430 ──────────────────────────────────────────────────────────────────

    def focusout_entry1430(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1430(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1430(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1430(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1430(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1430(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1430(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1430(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1430(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1430(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1430(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1430(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1430(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1430(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1430(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1430(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1440 ──────────────────────────────────────────────────────────────────

    def focusout_entry1440(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1440(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1440(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1440(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1440(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1440(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1440(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1440(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1440(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1440(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1440(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1440(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1440(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1440(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1440(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1440(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#region ─── Row_1450 ──────────────────────────────────────────────────────────────────

    def focusout_entry1450(self, event=None):
        self.entry(self.getMethodID())

    def down_entry1450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_entry1450(self, event=None):
        self.entry(self.getMethodID())

    def left_entry1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_entry1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_entry1450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmin1450(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def down_scmin1450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmin1450(self, event=None):
        self.entrymin(self.getMethodID(), self.getMethodID())

    def left_scmin1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmin1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmin1450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_lolim1450(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def down_lolim1450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_lolim1450(self, event=None):
        self.entrylow(self.getMethodID(), self.getMethodID())

    def left_lolim1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_lolim1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_lolim1450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def eventscale1450(self, scale_value):
        self.scale(self.getMethodID(), self.getMethodID())

    def left_scale1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scale1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def focusout_hilim1450(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def down_hilim1450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_hilim1450(self, event=None):
        self.entryhigh(self.getMethodID(), self.getMethodID())

    def left_hilim1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_hilim1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_hilim1450(self, event=None):
        self.incscale(self.getMethodID(), 1)

    def focusout_scmax1450(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def down_scmax1450(self, event=None):
        self.incscale(self.getMethodID(), -1)

    def entered_scmax1450(self, event=None):
        self.entrymax(self.getMethodID(), self.getMethodID())

    def left_scmax1450(self, event=None):
        self.incscale(self.getMethodID(), -0.1)

    def right_scmax1450(self, event=None):
        self.incscale(self.getMethodID(), 0.1)

    def up_scmax1450(self, event=None):
        self.incscale(self.getMethodID(), 1)

#endregion

#endregion
