# simulatorTS2
# Cryo Controls Simulator
European Spallation Source, Lund

Author: Gabor Fenyvesi, Krisztian Loki, Emilio Asensi

## NOTE

Simulator for TS2 with functions of petenv (https://gitlab.esss.lu.se/icshwi/petenv)

Cryo Controls Simulator is intended to replicate the functionality of field devices in the cryogenic circuits in a very simplistic way, by manually modifying the sensors values and actuators position to monitor the automatic process logic response without affecting the production systems.

It uses a python environment (petenv) to send desired sensors & actuators values to the test PLC (previously configured) as inputs. The test PLC has to be connected to a test IOC and propagating the EPICS pvs to the Simulation Sandbox (previously configured) for the Control & Command from the test OPI.

The testing system, including PLC and OPI, are intended to replicate the exact behaviour and configuration as the production system.

## Quickstart

Cryo Controls Simulator requires:
* Python 3.0
* tkinter
* pygubu
* setuptools
* pip3
* pkg_resources
* opcua

#### Launch the Virtual Machine for Simulation with NoMachine using one of the instances:

* cryo-opi-testsandbox.cslab.esss.lu.se
* cryo-opi-testsandbox-02.cslab.esss.lu.se

#### Execute the petenv simulator located at /SIM/ :
`python3 TS2_SIM_v7.py plcvar.txt yes`
or
`python3 TS2_SIM_v7.py plcvar.txt no`
* Use the txt file with the pv list saved for the initial state desired.
* Open Phoebus for OPI Control & Command as usual.
* Use yes or no argument to select the behaviour of snapshooted values readback

#### The IP of PLC is hardcoded into :
    TS2_SIM_v7.py
#   in row:
    client = OPCClient("172.30.6.222", timeout=10)  

#### Check the process logic
According to https://confluence.esss.lu.se/display/ATS/Operating+modes+logic+free+text


## Development
To have a new installation or if the production system has been through a PLCFactory update.

#### Generate TIA blocks from production CCDB to Simulator PLC:
`python plcfactory.py --device TS2-010CRM:Cryo-PLC-001 --plc-siemens --plc-hostname=ts2-crs-sim-plc.cslab.esss.lu.se --ioc --no-ioc-git`

#### Using manually created IOC files:

Upload the IOC files manually to the test IOC repository `https://gitlab.esss.lu.se/iocs/lab/ts2-010-sc-ioc-001`.

## Install the petenv dependencies

#### Python 3.0

* Step 1: Update the environment:
`yum update -y`
* Step 2: Install Python 3:
`yum install -y python3`

#### Pip
`yum install python3-pip -y`

#### Tkinter
`sudo yum install python3-tkinter`

#### Pygubu
`pip3 install pygubu`

#### Opcua
`pip3 install opcua`

#### EPICS
`pip3 install pyepics`

#### Numpy
`pip3 install numpy`

#### Libssl
`sudo dnf install redhat-rpm-config gcc libffi-devel python3-devel \ openssl-devel cargo`

#### Cryptography

* Step 1: Add the EPEL Repository:
`yum install epel-release`
* Step 2: Install pip or upgrade:
`yum install python-pip3` ;
`pip3 install --upgrade pip`
* Step 3: Install cryptography:
`pip3 install cryptography`
